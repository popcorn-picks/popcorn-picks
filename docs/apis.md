# APIs

## Users

- Methods: POST (Create User), GET (Get User/ All Users), PUT (Update User), DELETE (Delete User)
- Path: /api/users, /api/users/{username}

### Input:

```
{
    "first_name": string,
    "last_name": string,
    "username": string,
    "profile_pic": string,
    "date_joined": date,
    "password": string,
    "active": bool
}
```

### Output:

```
{
    "id": int,
    "first_name": string,
    "last_name": string,
    "username": string,
    "profile_pic": string,
    "date_joined": date,
    "active": bool
}
```

Creating a new User requires the first name, last name, username, profile picture, the date joined, and the active status. Upon creation, a new instance of a User will appear in the database.

## Movies

- Methods: GET (Get Movie/ All Movies), POST (Create Movie), PUT (Update Movie), DELETE (Delete Movie)
- Path: /api/movies, /api/movies/{movie_id}

### Input:

```
{
    "title": string,
    "picture": string,
    "description": string,
    "release_date": string,
    "upload_date": string,
    "rating": string,
}
```

### Output:

```
{
    "id": int,
    "title": string,
    "picture": string,
    "description": string,
    "release_date": date,
    "upload_date": date,
    "rating": string,
    "added_by": int

}
```

The Movies API will create, update, and delete movies. You can retrieve information about a specific movie or a list of all movies.

## Reviews

- Methods: POST (Create Review), GET (Get Review/ All Reviews), PUT (Update Review), DELETE (Delete Review)
- Path: /api/reviews, /api/reviews/{review_id}

### Input:

```
{
    "title": string,
    "review": strting,
    "rating": string,
    "date_added": date,
    "movie_id": int
}
```


### Output:

```
{
    "id:: int,
    "title": string,
    "review": string,
    "rating": string,
    "date_added": date,
    "user_id": int,
    "movie_id": int
}
```

The Reviews API allows users to submit reviews for movies. You can retrieve specific reviews or a list of all reviews.

## Comments

- Methods: POST (Create Comment), GET (Get All Comments), PUT (Update Comment), DELETE (Delete Comment)
- Path: /api/comments, /api/comments/{comment_id}

### Input:

```
{
    "comment": string,
    "user_id": int,
    "date_time": datetime,
    "review_id": int
}
```

### Output:

```
{
    "id": int,
    "comment": string,
    "user_id": int,
    "date_time": datetime,
    "review_id": int
}
```

Users can post comments on movie reviews. Retrieve specific comments or a list of all comments.

## Directors

- Methods: POST (Create Director), GET (Get All Directors)
- Path: /api/directors

### Input:

```
{
    "director_name": string,
    "movie_id": int
}
```

### Output:

```
{
    "id": int,
    "director_name": string,
    "movie_id": int
}
```

The Directors API manages information about movie directors. Retrieve specific director details or a list of all directors.

## Cast

- Methods: POST (Create Actor), GET (Get All Actors)
- Path: /api/cast

### Input:

```
{
    "actor_name": string,
    "movie_id": int
}
```

### Output:

```
{
    "id": int,
    "actor_name": string,
    "movie_id": int
}
```

The Cast API manages information about movie cast members. Retrieve specific cast details or a list of all cast members.