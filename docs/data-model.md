# Data Models

## PostgreSQL Database Tables

### Users

| Name          | Type    | Unique | Optional |
| ------------- | ------- | ------ | -------- |
| id            | int     | yes    | no       |
| first_name    | string  | no     | no       |
| last_name     | string  | no     | no       |
| username      | string  | yes    | no       |
| profile_pic   | string  | no     | no       |

### Movies

| Name          | Type    | Unique | Optional |
| ------------- | ------- | ------ | -------- |
| id            | int     | yes    | no       |
| title         | string  | no     | no       |
| picture       | string  | no     | no       |
| description   | string  | no     | no       |
| release_date  | date    | no     | no       |
| upload_date   | date    | no     | no       |
| rating        | references created type | no     | no       |
| added_by      | references to user entity | no     | no     |

### Reviews

| Name          | Type    | Unique | Optional |
| ------------- | ------- | ------ | -------- |
| id            | int     | yes    | no       |
| title         | string  | no     | no       |
| review        | string  | no     | no       |
| rating        | string  | no     | no       |
| date_added    | date    | no     | no       |
| user_id       | references to user entity    | no     | no    |
| movie_id      | references to movie entity | no  | no  |

### Comments

| Name          | Type    | Unique | Optional |
| ------------- | ------- | ------ | -------- |
| id            | int     | yes    | no       |
| comment       | string  | no     | no       |
| user_id       | references to user entity  | no     | no    |
| date_time     | datetime  | no     | no       |
| review_id    | references to review entity    | no     | no    |

### Cast

| Name          | Type    | Unique | Optional |
| ------------- | ------- | ------ | -------- |
| id            | int     | yes    | no       |
| actor_name    | string  | no     | no       |
| movie_id      | references to movie entity  | no     | no    |

### Directors

| Name          | Type    | Unique | Optional |
| ------------- | ------- | ------ | -------- |
| id            | int     | yes    | no       |
| director_name | string  | no     | no       |
| movie_id      | references to movie entity | no     | no    |
