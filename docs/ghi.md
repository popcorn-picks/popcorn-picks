# Popcorn Picks Front-End Web Interface

## Home Page

After a User has successfully created an account / logged into an existing account, they will be directed to this page. There will be a section that highlights the latest movies added to the website as well as a brief summary/overview of the application.

![HomePage](wireframes/home-page.png)

## Signup Page

The Signup page will allow the User to create an account, which will ultimately allow additional access to the application. Most functionalities are provided to Users with an account who are Signed in.

![SignupPage](wireframes/singup-page.png)

## Signin Page

The Signin page will allow the User to log into their account, which will allow additional access to the application.

![SigninPage](wireframes/signin-page.png)

## Create Movie Form

Users who are logged in have access listing movies through the movie form. The button to navigate to this form is found in the movie list page. It provides the necessary fields to be able to create a movie.

![MovieForm](wireframes/movie-form.png)

## Movie List

Users can navigate to the movie list page which contains all the movies that have been created by the users of the application. This page is navigated through the "Movies" button on the Nav bar.

![MovieList](wireframes/movie-list.png)

## Movie Detail

Once a User is in the movie list page, they have the ability to click on any movie they want and be navigated to the detail page of that movie. The detail page contains further information about the chosen movie, including the title, release date, director, cast list, popcorn rating, and the genre. It also contains reviews that other users have posted about the movie. Users also have the ability to comment on these reviews.

![MovieDetail](wireframes/movie-detail.png)

## Review Form

In the movie detail page, users can click the "add review" button and be navigated to a form that upon completion, will create a review on the movie chosen for other users to see.

![ReviewForm](wireframes/review-form.png)

## Comment on Review

This page will allow Users to comment on reviews. Comments enhance the application by allowing communication between users. They have the ability to share their opinions and thoughts on movies!

![CommentForm](wireframes/comment-form.png)

## Profile Page

Users can navigate to their own profile page which allows them to view their reviews. In the profile page, users will also have the option to delete or edit their profile.

![ProfilePage](wireframes/profile-page.png)