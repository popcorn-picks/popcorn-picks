01/12/2024
This week, the Popcorn Picks team efficiently mapped out the project. We worked together to correctly set a foundation for ourselves to be able to have a great starting point. 

We started off by creating a map design of how we wanted our website to look like to getg a better understanding as to how we should approach the project.

Additionally, we engaged in basic domain modeling and generated issues on GitLab. Our efficient progress allowed us to conduct an early project review. We used the extra time to refine our project planning documents and to deepen our understanding of FastAPI.

The first week coming to an end, we fell as if we have a great foundation set and are ready to tackle the world of coding and start actually creating the backend for the issues we have created and assigned to oursleves.
