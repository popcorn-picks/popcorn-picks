## Journals

Please keep all of your individual journals in this directory.

Each team member is required to keep a development journal, which should be a single markdown file with an entry for each day the project was worked on.

Recommended naming convention is `first_last.md`.


Fri Jan 12

This week the Popcorn Picks team planned out the project by creating wireframing and endpoint docs. We also did some light domain modeling and then created issues on gitlab. Since we were ahead of schedule, we had our project review early and took time to polish up our documents for project planning and reviewed the explorations for FastAPI. Our goal is to get a better grasp on the material we will be using for the project and then build the tables for our database.


Fri Jan 19

This week the Popcorn Picks team started the 4 day week by creating our tables for the database. We chose SQL for our database and then proceeded to create all of our tables and mob program our first endpoint which was the POST for create user and we successfully merged into our development branch. Overnight we created two more POST endpoints for our other features that are ready to be merged. We then started to mob program our backend authentication but hit some errors that halted our progress. As of now were are still on track with Rosheen's checkpoints for week 14.


Fri Jan 29

This week the team finished the backend endpoints and applied authentication to them. We also started on the frontend pages and have 3 done. My particular part is the Movies list page. For the coming week, we want our authentication and authorization done and significant progress to be made in our frontend pages.


Thurs Feb 1

This week we deployed our backend and continued to work on frontend pages. Almost all of the mvp is done. What is left is my part for the CommentForm.js, unit tests and deploying our frontend. The CommentForm is almost done, and my backend unit test is written but getting ModuleNotFound errors that I need to debug. Other than that, progress on the project is going very well. For the coming week we will be writing the rest of our unit tests and work on deploying the front end.
