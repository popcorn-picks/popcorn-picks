## Friday 1/12/24:

We have spent the last week wireframing, desiging our API endpoints, and establishing our MVPs. I think we have a solid idea for our project's design and concept. Today we are going to apply the changes to our design that Rosheen suggested and when that is done we can begin creating our databases.

Things that need attention:
- after getting a stronger grasp on FastAPI, I think it's important that we clarify the structure of our request and response data. This will make programming different features asynchronously easier and consistent.
- I am most nervous about creating our databases today, because we might miss a necessary property/column and then have to take delete our volume and rebuild. Out of past experiences, this seems to be what I have had issues with before.

## Friday 1/19/2024

We finally were able to succesfully implement the create users function with authentication. We started it in the afternoon on Thursday and we weren't able to figure it out until this evening. We, fortunately, had some assistance from our SEIR, Alex, who reminded us that we can easily access the source code for the jwt-for-fast-api library by right clicking on its methods on vscode. I think that this will be a very helpful tip even outside of that library, when we are not understanding why something we are doing is not working. Our team originally attempted to mob program on Thursday, but we tried a different approach to team programming today. Troy, Roland, and I all broke off into different branches to try to solve the authentication issue individually, while Harut worked on the post movies endpoints, since the rest of our features relies on it. I think that we have made great progress so far and have learned a lot. We are getting to understand our work flow and the material, so I think it will get better from here.

Things that need attention:
- finish full CRUD operations for users
- finish POST for movies, comments, reviews to ensure that the tables are properly connected
    - that way we can start making SQL queries to our database

## Friday 1/26/2024

We finished all of the endpoints that we need this week and we have gotten started on our frontend. Right now, my biggest blocker is figuring out how to access protected endpoints from the frontend, in development. I am going to look over the material to see if there is something I am not grasping to implement this properly. I think the rest of my team and I are making good progress, but we are struggling every now and then on topics that we haven't necessarily fully grasped yet.

Things that need attention:
- Figuring out how to access protected API endpoints to be able to display data on the frontend
- Finish assigning frontend issues to team members
- Understand and apply unit tests

## Friday 2/2/2024

We have completed most of the frontend components for our site. It is essentially fully functional without interferance on our end. We also were able to successfully deploy our backend and all that is left is for us to each apply unit tests and deploy our frontend. I think at this point we can start implementing stretch goals, refactoring to make our code more efficient and neat, adding front end error handling and adding components to make the user experience more pleasant(like adding messages to let the user know that they didn't fill out a field). I am happy with the progress that we've made so far and I think we will have a good product to turn in by the end of the week.
