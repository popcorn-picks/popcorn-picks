WEEKLY JOURNAL ENTRIES


Journal Entry - Week 1

Date: 01/08/24 - 01/12/24

This week was the start of an exciting journey into making Popcorn Picks, an awesome movie rating website. As a team, we kicked things off by planning how the website will look and work by creating wireframes and establishing API endpoints.

Our initial focus revolved around planning the user interface, and we translated our visions into detailed wireframes. This allowed us to visualize the layout and functionality of Popcorn Picks, ensuring a user-friendly design.

We also set up the foundation for our database by creating API endpoints. The mapping of our endpoints gave us a grasp on how the backend will be sorted out/ deivided between the team.

Collaboration with the team was streamlined through GitLab, where we assigned our tracking issues. This approach enhanced our project management and also gave us a sence of accountability among team members.

We dedicated time to mapping out our Minimum Viable Product (MVP) and stretch goals. This allowed us to prioritize features and functionalities, ensuring that we deliver a core product that meets our users' needs while leaving room for improvement.

Looking back, it's clear that our teamwork and planning have set a strong foundation. 


Journal Entry - Week 2

Date: 01/15/24 - 01/19/24

This week our team made some good progress regarding the backend portion of our application.

First we decided to implement SQL as the database setup. Then we created all of our database tables that we needed in our application. We successfully created a users, movies, reviews, comments, cast, and directors table which we then migrated and verified through BeeKeeper. After initiating the tables, we then tackled the user endpoint for creating an instance of a user in our queries and router directories as a group.

We are currently in the process of implementing the authentication in our application. This will allow certain features only accessable to users that are authenticated.

So far, we are successfully able to attain the checkpoint for this week. However, I can also personally say that this week was definitely more challenging than the week prior because of certain blockers that slowed us down. A big portion of our blockers involved git related issues when it came to merging and some of it involved authentication. I'm certain that we will continue mmaking progress with our application and eventually start working on the front-end portion soon.


Journal Entry - Week 3

Date: 01/22/24 - 01/26/24

This week our team successfully completed our API endpoints as well as protecting the ones that need authentication.

Since our backend and database is done, we finally began working on the frontend web pages. So far, we've completed the Homepage, the Login page, as well as the Sign up page.

Looking forward, we will continue working on our frontend pages coming into next week, as well as also applying frontend Auth to those pages.


Journal Entry - Week 4

Date: 01/29/24 - 02/02/24

This week our was able to successfully implement unit testing for our api endpoints as well as deploy our backend. On top of all that, we also worked on polishing up some of our frontend pages. So far it seems like we've met all of our MVP, sometime next week we will go over the rubric and double check if we hit all the criteria. I'm happy with how this project is going so far. This week has been really productive.