import { useEffect, useState } from 'react';
import { useAuthContext } from "@galvanize-inc/jwtdown-for-react";
import { useParams, useNavigate } from 'react-router-dom';
import { RiDeleteBin6Fill } from "react-icons/ri";



function EditMovie() {
    const [movie, setMovie] = useState();
    const [tempMovie, setTempMovie] = useState();
    const { token } = useAuthContext();
    const params = useParams();
    const movie_id = params.movie_id;
    const navigate = useNavigate();


    const getMovieDetails = async() => {
        const response = await fetch(`${process.env.REACT_APP_API_HOST}/api/movies/${movie_id}`, {
            headers: {Authorization: `Bearer ${token}`}
        });
        if (response.ok) {
            const data = await response.json();
            setMovie(data);
            setTempMovie(data);
        }
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
    useEffect(() => {getMovieDetails()}, [token]);


    const handleSubmit = async (e) => {
        e.preventDefault();


        const updated_data = {
            title: movie.title,
            picture: movie.picture,
            description: tempMovie.description,
            release_date: tempMovie.release_date,
            upload_date: movie.upload_date,
            rating: tempMovie.rating
        }

        try {
            const response = await fetch(`${process.env.REACT_APP_API_HOST}/api/movies/${movie_id}`, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`
                },
                body: JSON.stringify(updated_data),
            });
            if (response.ok) {
                navigate(`/movies/${movie_id}`)
            }
        } catch (error) {
            console.log("Unable to submit update formdue to error:", error)
        }
    }

    const handleDelete = async(e) => {
        const response = await fetch(`${process.env.REACT_APP_API_HOST}/api/movies/${movie_id}`, {
                method: 'delete',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`
                }
        })
        if(response.ok) {
            navigate("/movies")
        }
    }

    if(movie) {
        return (
        <>
        <div className="container">
            <div className="row">
                <div className="col col-lg-4 py-5">
                    <div className="card mb-3 sticky-top">
                        <img className="card-img-top" src={movie.picture} alt={movie.title} />
                        <div className="card-body">
                            <h4 class="card-title d-flex justify-content-center align-items-center">
                                {movie.title}
                                <button type="button" className="btn p-0" data-toggle="modal" data-target="#exampleModal">
                                    <RiDeleteBin6Fill
                                        color= "#FF0000"
                                        size= {30}
                                        title={`delete ${movie.title}?`}
                                    />
                                </button>
                            </h4>
                        </div>
                    </div>
                </div>
                <div className="col py-5">
                    <div className="container">
                        <h3 className="mb-3">Edit movie details:</h3>
                        <form>
                            <div className="form-floating mb-3">
                                <label htmlFor="release_date" className="form-label">Release Date:</label>
                                <input
                                    value= {tempMovie.release_date}
                                    onChange={(e) => {
                                        setTempMovie({
                                            ...tempMovie,
                                            release_date: e.target.value
                                        });
                                    }}
                                    type="date"
                                    name="release_date"
                                    id="release_date"
                                    className="form-control"
                                />
                            </div>
                            <div className="form-floating mb-3">
                                <label htmlFor="rating" className="form-label mr-2">Rating:</label>
                                <select
                                    value={tempMovie.rating}
                                    onChange={(e) => {
                                        setTempMovie({
                                            ...tempMovie,
                                            rating: e.target.value
                                        })
                                    }}
                                    name="rating"
                                    id="rating"
                                    className="form-select"
                                >
                                    <option defaultValue={"Select Rating"}>Select rating</option>
                                    <option value="G">G</option>
                                    <option value="PG">PG</option>
                                    <option value="PG-13">PG-13</option>
                                    <option value="R">R</option>
                                    <option value="NC-17">NC-17</option>
                                </select>
                            </div>
                            <div className="mb-3">
                                <label htmlFor="description" class="form-label">Description</label>
                                <textarea
                                    value= {tempMovie.description}
                                    onChange={(e) => {
                                        setTempMovie({
                                            ...tempMovie,
                                            description: e.target.value
                                        })
                                    }}
                                    name="description"
                                    className="form-control"
                                    id="description"
                                    type="text"
                                    rows="3">
                                </textarea>
                            </div>
                            <button className="btn btn-primary" onClick={handleSubmit}>Save changes</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        {/* Modal */}
        <div className="modal" id="exampleModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div className="modal-dialog" role="document">
                <div className="modal-content text-center p-4">
                    <h5 className="modal-title">Are you sure you want to delete:</h5>
                    <h1>{movie.title}</h1>

                    <div className='d-flex justify-content-center mt-3'>
                        <button type="button" className="btn btn-secondary m-2" data-dismiss="modal">Cancel</button>
                        <button type="button" className="btn btn-danger m-2" data-dismiss="modal" onClick={handleDelete}>Delete</button>
                    </div>

                </div>
            </div>
        </div>
        </>
        )
    }
}



export default EditMovie
