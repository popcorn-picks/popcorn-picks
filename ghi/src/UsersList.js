import React, { useEffect, useState } from "react";
import { useAuthContext } from "@galvanize-inc/jwtdown-for-react";
import { Link } from 'react-router-dom';

function UsersList() {
  const [users, setUsers] = useState([]);
  const [searchTerm, setSearchTerm] = useState("");
  const { token } = useAuthContext();

  const loadUsers = async () => {
    try {
      const response = await fetch(`${process.env.REACT_APP_API_HOST}/api/users/`, {
        method: 'GET',
        credentials: "include",
        headers: {
          'Content-Type': 'application/json',
        },
      });

      if (!response.ok) {
        throw new Error('Failed to fetch users');
      }

      const data = await response.json();
      setUsers(data.users);
    } catch (error) {
      console.error("Error loading users:", error.message);
    }
  };

  useEffect(() => {
    loadUsers();
  }, [token]);

  const handleSearchChange = (e) => {
    setSearchTerm(e.target.value);
  };

  const filteredUsers = users.filter(user =>
    user.username.toLowerCase().includes(searchTerm.toLowerCase())
  );

  return (
    <div>
      <input
        type="text"
        className="form-control mt-4"
        placeholder="Search users..."
        value={searchTerm}
        onChange={handleSearchChange}
      />
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Users</th>
          </tr>
        </thead>
        <tbody>
          {filteredUsers.map((user) => (
            <tr key={user.id}>
              <Link to={`/users/${user.username}`}><td>{user.username}</td></Link>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default UsersList;
