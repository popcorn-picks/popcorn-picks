/* eslint-disable eqeqeq */
import { useEffect, useState } from 'react';
import { useAuthContext } from "@galvanize-inc/jwtdown-for-react";
import { useParams, Link, useNavigate } from 'react-router-dom';
import PopcornRating from './PopcornRating';
import MovieReviews from './MovieReviews';
import { format } from 'date-fns';
import { FiEdit } from "react-icons/fi";

function MovieDetails() {
    const [movie, setMovie] = useState();
    const { token } = useAuthContext();
    const params = useParams();
    const movie_id = params.movie_id;
    const navigate = useNavigate();
    const [reviews, setReviews] = useState([]);
    const [users, setUsers] = useState("");
    const [creator, setCreator] = useState(false);


    const getUsers = async () => {
        try {
            const response = await fetch(`${process.env.REACT_APP_API_HOST}/api/users`, {
                headers: { Authorization: `Bearer ${token}` }
            });

            if (response.ok) {
                const data = await response.json();
                setUsers(data.users);
            } else {
                throw new Error("Failed to fetch user data");
            }
        } catch (error) {
            console.error("Error fetching user data:", error.message);
        }
    };

    useEffect(() => {
        getUsers();
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [reviews]);

    function getUser(added_by) {
        for (const user of users) {
            if (user.id === added_by) {
                return user.username;
            }
        }
    }

    const getMovie = async () => {
        try {
            const response = await fetch(`${process.env.REACT_APP_API_HOST}/api/movies/${movie_id}`, {
                headers: { Authorization: `Bearer ${token}` }
            });

            if (response.ok) {
                const data = await response.json();
                setMovie(data);
            } else {
                throw new Error("Failed to fetch movie data");
            }
        } catch (error) {
            console.error("Error fetching movie data:", error.message);
        }
    };

    useEffect(() => {
        getMovie();
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [token]);

    const getReviews = async () => {
        try {
            const response = await fetch(`${process.env.REACT_APP_API_HOST}/api/reviews/`, {
                headers: { Authorization: `Bearer ${token}` }
            });

            if (response.ok) {
                const data = await response.json();
                let review_array = [];
                for (const review of data.reviews) {
                    if (review.movie_id == movie_id) {
                        review_array.push(review);
                    }
                }
                setReviews(review_array);
            } else {
                throw new Error("Failed to fetch reviews data");
            }
        } catch (error) {
            console.error("Error fetching reviews data:", error.message);
        }
    };

    const getToken = async() => {
        try {
            const response = await fetch(`${process.env.REACT_APP_API_HOST}/token`, {
                credentials: "include"
            });
            if (!response.ok) {
                throw new Error('Failed to fetch token data');
            }
            const data = await response.json();
            const account = data["account"];
            if (account.id == movie.added_by) {
                setCreator(true)
            }
        } catch (error) {
            console.log("Failed to fetch token data", error)
        }
    };

    useEffect(() => {
        getReviews();
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [movie]);

    useEffect(() => {
        getToken();
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [token, movie]);


    if(movie) {
        return (
        <div class="container">
            <div class="row">
                <div class="col col-lg-4 py-5">
                    <div class="card mb-3 sticky-top">
                        <img class="card-img-top" src={movie.picture} alt={movie.title} />
                        <div class="card-body">
                            <h4 class="card-title d-flex justify-content-between align-items-center">
                                {movie.title}
                            {creator && (
                                <Link to={`/movies/${movie.id}/edit`} title="edit movie details">
                                    <FiEdit
                                        size={20}
                                    />
                                </Link>
                            )}

                            </h4>
                                <h6 class="card-subtitle mb-2 text-body-secondary">Released: { format(movie.release_date, 'MMMM d, yyyy') }</h6>
                                <h6 class="card-subtitle mb-2 text-body-secondary mt-2">Rating: {movie.rating}</h6>
                                <figcaption class="blockquote-footer m-2">
                                    Added by <Link to={`/users/${getUser(movie.added_by)}`}>{getUser(movie.added_by)}</Link>
                                </figcaption>
                                <PopcornRating
                                    reviews = {reviews}
                                />
                            <p class="card-text">{movie.description}</p>
                            <p class="card-text"><small class="text-muted"></small></p>
                        </div>
                    </div>
                </div>
                <div class="col py-5">
                <div class="container">
                    <button type="button" class="btn btn-outline-danger m-3" onClick={() => navigate(`/movies/${movie.id}/review`)}> Review "{movie.title}"</button>
                </div>
                    <MovieReviews
                        id = {movie_id}
                        reviews = {reviews}
                        users = {users}
                    />
                </div>
            </div>
        </div>
    )
} else {
    <p> You are not signed in. Please log in to access. </p>
}
    }

export default MovieDetails;
