import { useState, useEffect } from "react"
import { useAuthContext } from "@galvanize-inc/jwtdown-for-react";
import { format } from "date-fns";


const profile_pic_style = {

    width: "50px",
    height: "50px"

}

function Comments(props) {
    const [comments, setComments] = useState([]);
    const [allComments, setAllComments] = useState([]);
    const [users, setUsers]= useState([]);
    const token = useAuthContext()
    const { review_id, counter } = props


    const getAllComments = async() => {
        const response = await fetch(`${process.env.REACT_APP_API_HOST}/api/comments/`, {
            method: 'GET',
            credentials: 'include'
        });
        if(response.ok) {
            const data = await response.json();
            setAllComments(data.comments)
        }
    }

    const getComments = async() => {
        const response = await fetch(`${process.env.REACT_APP_API_HOST}/api/review_comments/${review_id}`, {
            method: 'GET',
            credentials: 'include'
    });
        if(response.ok) {
            const data = await response.json();
            setComments(data.review_comments)
        }
    }


    // function getReviewComments(review_id) {
    //     let reviewComments = [];
    //     for(const comment of comments) {
    //         // eslint-disable-next-line eqeqeq
    //         if(comment.review_id == review_id) {
    //             reviewComments.push(comment)
    //         }
    //     } return reviewComments;
    // }


    const getUsers = async() => {
        const response = await fetch(`${process.env.REACT_APP_API_HOST}/api/users`, {
            method: 'GET',
            credentials: 'include',
        });
            if(response.ok) {
                const data = await response.json()
                setUsers(data.users)
            }
    }


    function getUsername(user_id) {
        for (const user of users) {
            // eslint-disable-next-line eqeqeq
            if (user_id == user.id) {
                return user.username
            }
        }
    }

    function getUserProfilePicture(user_id) {
        for (const user of users) {
            // eslint-disable-next-line eqeqeq
            if (user_id == user.id) {
                return user.profile_picture
            }
        }
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
    useEffect(()=> {getAllComments()}, [token, counter])
    // eslint-disable-next-line react-hooks/exhaustive-deps
    useEffect(()=> {getComments()}, [allComments, counter])
    useEffect(() => {getUsers()}, [token])

    if(comments) {
        return (
            <>
            <div class="card card-body">
                {comments.map((comment) => {
                    return (
                        <div className="container d-flex mt-4" key={comment.id}>
                            <img alt={getUsername(comment.user_id)} className="rounded-circle shadow-4-strong m-0" style={profile_pic_style} key={comment.id} src={getUserProfilePicture(comment.user_id)}
                            />
                            <div className="container d-flex flex-column justify-content-center" key={comment.id} >
                                <p className="text-muted m-0">{getUsername(comment.user_id)} | {format(comment.date_time, 'M-d-yyyy')}</p>
                                <p>{comment.comment}</p>
                            </div>
                        </div>
                    )
                })}
            </div>
            </>
        )
    }
}

export default Comments
