import { useState } from 'react';
import Comments from './Comments';


function CommentForm(props) {
    const { review_id } = props
    const [comment, setComment] = useState("");
    const [error, setError] = useState(null);
    const [counter, setCounter] = useState(0)


    const handleSubmit = async (e) => {
        e.preventDefault();

        const data = {
            comment: comment,
            date_time: new Date().toISOString().split('T')[0],
            review_id: review_id
        };


        try {
            const response = await fetch(`${process.env.REACT_APP_API_HOST}/api/comments`, {
                method: 'POST',
                credentials: "include",
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(data)
            });

            if (response.ok) {
                setComment("");
                setCounter(counter+1)

            } else {
                throw new Error("Failed to post comment");
            }
        } catch (error) {
            console.error("Error posting comment:", error.message);
            setError("Failed to post comment. Please try again later.");
        }
    };

    const handleCommentChange = (e) => {
        const value = e.target.value;
        setComment(value);
    };


    if (error) {
        return (
            <div className='row'>
                <div className='offset-3 col-6'>
                    <div className='shadow p-4 mt-4'>
                        <p>Error: {error}</p>
                    </div>
                </div>
            </div>
        );
    }
    return (
        <>
        <div className="justify-content-center align-items-center mt-3">
            <div className="input-group mb-3">
                <input
                    onChange={handleCommentChange}
                    value={comment}
                    required
                    type="text"
                    name="comment"
                    id="comment"
                    className='form-control'
                />
                <div className="input-group-append">
                    <button className="btn btn-secondary" onClick={handleSubmit} type="button">comment</button>
                </div>
            </div>
            <Comments
                review_id={review_id}
                counter={counter}
            />

        </div>
        </>
        );
    }


export default CommentForm;
