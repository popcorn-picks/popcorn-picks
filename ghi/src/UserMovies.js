import {
  MDBCard,
  MDBCardBody,
  MDBCol,
}
from 'mdb-react-ui-kit';
import { format } from 'date-fns';


const moviePoster = {
    width: "150px",
    height: "100%"
}

function UserMovies(props) {
    const { user, movies } = props
    console.log("movies", movies)

    let userMovies = []
    for(const movie of movies) {
        // eslint-disable-next-line eqeqeq
        if(movie.added_by == user.id) {
            userMovies.push(movie)
        }
    }
    return(
        <>
            {userMovies.map((movie) => {
                // eslint-disable-next-line eqeqeq
                if(movie == userMovies[0]) {
                    return (
                        <MDBCard className="mb-4 border-top-0">
                            <MDBCardBody className="d-flex align-items-center">
                                <img
                                    src={movie.picture}
                                    alt={movie.title}
                                    style={moviePoster}
                                />
                                <MDBCol>
                                    <h2>{movie.title}</h2>
                                    <h6 className="text-muted">{ format(movie.release_date, 'M-dd-yyyy') }</h6>
                                </MDBCol>
                            </MDBCardBody>
                        </MDBCard>
                    )
                } else {
                    return (
                        <MDBCard className="mb-4">
                            <MDBCardBody className="d-flex align-items-center">
                                <img
                                    src={movie.picture}
                                    alt={movie.title}
                                    style={moviePoster}
                                />
                                <MDBCol>
                                    <h2>{movie.title}</h2>
                                    <h6 className="text-muted">{ format(movie.release_date, 'M-dd-yyyy') }</h6>
                                </MDBCol>
                            </MDBCardBody>
                        </MDBCard>
                    )
                }
            })}
        </>
    )
}

export default UserMovies;
