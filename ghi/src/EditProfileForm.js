import useToken from "@galvanize-inc/jwtdown-for-react";
import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { useParams } from "react-router-dom";

function EditProfileForm() {
    const [ formData, setFormData ] = useState({
        first_name: '',
        last_name: '',
        profile_pic: '',
        password: '',
        active: true,
        date_joined: "",
    });

    const navigate = useNavigate();
    const { token } = useToken();
    const params = useParams();
    const username = params.username;
    const [userId, setUserId] = useState(null);

    useEffect(() => {

        const fetchUserProfile = async () => {
            try {
                const response = await fetch(`${process.env.REACT_APP_API_HOST}/api/users/${username}`, {
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                });

                if (!response.ok) {
                    throw new Error("Failed to fetch user profile");
                }

                const userData = await response.json();

                setUserId(userData.id);

                setFormData({
                    ...userData,
                    active: true,
                });
            } catch (error) {
                console.error("Error fetching user profile:", error.message);
            }
        };

        fetchUserProfile();
    }, [token, username]);


    const handleChange = (e) => {
        const { name, value } = e.target;
        setFormData({
            ...formData,
            [name]: value,
        });
    };

    const handleSubmit = async (e) => {
        e.preventDefault();

        try {
            const response = await fetch(`${process.env.REACT_APP_API_HOST}/api/users/${userId}`, {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify(formData),
            });

            if (!response.ok) {
                throw new Error("Failed to update user profile");
            }
            navigate(`/users/${username}`);
        } catch (error) {
            console.error("Error updating user profile:", error.message);
        }
    };

    return (
            <div className="form-body">
            <div className="form-holder">
                <div className="form-content">
                <div className="form-items">
                    <h1>Edit Profile</h1>
                    <form onSubmit={handleSubmit} id="edit-profile-form">
                    <div className="col-md-12">
                        <label className="form-label" style={{marginBottom: '0px', marginTop: '5px'}}>First name:</label>
                        <input
                        onChange={handleChange}
                        value={formData.first_name}
                        required type="text"
                        name="first_name"
                        id="first_name"
                        className="form-control"
                        />
                    </div>
                    <div className="col-md-12">
                        <label className="form-label" style={{marginBottom: '0px', marginTop: '15px'}}>Last name:</label>
                        <input
                        onChange={handleChange}
                        value={formData.last_name}
                        required type="text"
                        name="last_name"
                        id="last_name"
                        className="form-control" />
                    </div>
                    <div className="col-md-12">
                        <label className="form-label" style={{marginBottom: '0px', marginTop: '15px'}}>Password:</label>
                        <input
                        onChange={handleChange}
                        value={formData.password}
                        required type="password"
                        name="password"
                        id="password"
                        className="form-control" />
                    </div>
                    <div className="col-md-12">
                        <label className="form-label" style={{marginBottom: '0px', marginTop: '15px'}}>Profile Picture:</label>
                        <input
                        onChange={handleChange}
                        value={formData.profile_pic}
                        required type="text"
                        name="profile_pic"
                        id="profile_pic"
                        className="form-control" />
                    </div>
                    <div>
                        <input
                        className="btn btn-primary" style={{ margin: '20px 10px 10px 15px' }}
                        type="submit"
                        value="Submit"
                        onClick={handleSubmit}
                        />
                    </div>
                    </form>
                </div>
                </div>
                </div>
            </div>
    );
}

export default EditProfileForm;
