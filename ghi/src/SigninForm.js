import useToken, { useAuthContext } from "@galvanize-inc/jwtdown-for-react";
import { useEffect, useState } from "react";
import { useNavigate, Link } from "react-router-dom";

const LoginError = () => {
  return (
    <div className="alert alert-danger my-3" role="alert">
      Username or password is incorrect. <Link to="/signup" className="alert-link">Click to sign up</Link>.
    </div>
  );
};

const SigninForm = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const { login } = useToken();
  const navigate = useNavigate();
  const { token } = useAuthContext();
  const [loginFail, setLoginFail] = useState(false);
  const [firstLoad, setFirstLoad] = useState(false);

  const handleSubmit = (e) => {
    e.preventDefault();
    login(username, password);
  };

  useEffect(() => {
    if (token) {
      navigate("/");
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [token]);

  useEffect(() => {
    if (firstLoad) {
      setLoginFail(true);
    }
  }, [firstLoad]);

  const handleFirstLoad = () => {
    setTimeout(() => {
      setFirstLoad(true);
    }, 500);
  };

return (
  <>
    <div className="form-body">
      <div className="form-holder">
        <div className="form-content">
          <div className="form-items">
            <h3>Sign In</h3>
            <form onSubmit={(e) => handleSubmit(e)} id="create-user-form">
              <div className="col-md-12">
                <input
                  placeholder="Username"
                  onChange={(e) => setUsername(e.target.value)}
                  required
                  type="text"
                  name="username"
                  id="username"
                  className="form-control"
                />
                <div className="valid-feedback">Username is valid!</div>
                <div className="invalid-feedback">
                  Username cannot be blank!
                </div>
              </div>
              <div className="col-md-12">
                <input
                  placeholder="Password"
                  onChange={(e) => setPassword(e.target.value)}
                  required
                  type="password"
                  name="password"
                  id="password"
                  className="form-control"
                />
                <div className="valid-feedback">Password is valid!</div>
                <div className="invalid-feedback">
                  Password cannot be blank!
                </div>
              </div>
              <div>
                <input
                className="btn btn-primary" style={{ margin: '20px 10px 10px 15px' }}
                type="submit"
                value="Login"
                onClick={handleFirstLoad}
                />
              </div>
                {loginFail && <LoginError />}
            </form>
          </div>
        </div>
      </div>
    </div>
  </>
);
  };
  
  export default SigninForm;