import useToken from "@galvanize-inc/jwtdown-for-react";
import { useState } from "react";
import { useNavigate } from "react-router-dom";


function SignupForm() {
    const [ formData, setFormData ] = useState({
        first_name: '',
        last_name: '',
        username: '',
        profile_pic: '',
        password: '',
        confirmPassword: '',
        active: true,
    });
    const [errorMessage, setErrorMessage] = useState("");
    const navigate = useNavigate();
    const { login } = useToken();

    const handleChange = (e) => {
        const { name, value } = e.target;
        setFormData({
            ...formData,
            [name]: value,
        });
    };

    const handleSubmit = async (e) => {
        e.preventDefault();

        const currentDate = new Date();
        formData.date_joined = currentDate.toLocaleDateString('en-CA');

        if (formData.password !== formData.confirmPassword) {
            setErrorMessage("Passwords do not match");
            return;
        }

        try {
            const response = await fetch(`${process.env.REACT_APP_API_HOST}/api/users`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(formData),
            });

            if (!response.ok) {
                throw new Error('Failed to create user');
            }

            setFormData({
                first_name: '',
                last_name: '',
                username: '',
                profile_pic: '',
                password: '',
                confirmPassword: '',
                active: true,
            });

            login(formData.username, formData.password);
            navigate("/");

        } catch (error) {
            console.error('Error creating user:', error.message);
        }
    };

    return  (
    <>
      <div className="form-body">
        <div>
          <div className="form-holder">
            <div className="form-content">
              <div className="form-items">
                <h3>Register Today</h3>
                <p>Fill in the data below.</p>
                <form onSubmit={handleSubmit} id="create-user-form">
                  <div className="col-md-12">
                    <input
                      placeholder="First Name"
                      onChange={handleChange}
                      value={formData.first_name}
                      required type="text"
                      name="first_name"
                      id="first_name"
                      className="form-control"
                    />
                    <div className="valid-feedback">First name is valid!</div>
                    <div className="invalid-feedback">
                      First name cannot be blank!
                    </div>
                  </div>

                  <div className="col-md-12">
                    <input
                      placeholder="Last Name"
                      onChange={handleChange}
                      value={formData.last_name}
                      required type="text"
                      name="last_name"
                      id="last_name"
                      className="form-control" />
                    <div className="valid-feedback">Last name is valid!</div>
                    <div className="invalid-feedback">
                      Last name cannot be blank!
                    </div>
                  </div>

                  <div className="col-md-12">
                    <input
                      placeholder="Username"
                      onChange={handleChange}
                      value={formData.username}
                      required type="text"
                      name="username"
                      id="username"
                      className="form-control" />
                    <div className="valid-feedback">Username is valid!</div>
                    <div className="invalid-feedback">
                      Username cannot be blank!
                    </div>
                  </div>

                  <div className="col-md-12">
                    <input
                      placeholder="Password"
                      onChange={handleChange}
                      value={formData.password}
                      required type="password"
                      name="password"
                      id="password"
                      className="form-control" />
                    <div className="valid-feedback">Password is valid!</div>
                    <div className="invalid-feedback">
                      Password cannot be blank!
                    </div>
                  </div>

                  <div className="col-md-12">
                    <input
                      placeholder="Confirm Password"
                      onChange={handleChange}
                      value={formData.confirmPassword}
                      required
                      type="password"
                      name="confirmPassword"
                      id="confirmPassword"
                      className="form-control"
                    />
                    <div className="valid-feedback">Passwords match!</div>
                    <div className="invalid-feedback">
                      Passwords do not match!
                    </div>
                  </div>
                  
                  <div className="col-md-12">
                     <input
                      placeholder="Profile picture"
                      onChange={handleChange}
                      value={formData.profile_pic}
                      required type="text"
                      name="profile_pic"
                      id="profile_pic"
                      className="form-control" />
                    <div className="valid-feedback">Profile picture is valid!</div>
                    <div className="invalid-feedback">
                      Profile picture cannot be blank!
                    </div>
                  </div>
                  {errorMessage && (
                    <div className="col-md-12 text-danger">{errorMessage}</div>
                  )}

                  <div className="form-button mt-3" style={{ margin: '20px 10px 10px 15px' }}>
                    <button
                      className="btn btn-primary"
                    >
                      Register
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default SignupForm;