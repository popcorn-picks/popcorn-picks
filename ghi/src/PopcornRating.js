import { GiPopcorn } from "react-icons/gi";


function PopcornRating(props) {
    const { reviews } = props

    let total=0;
    for (const review of reviews) {
        let rating = Number(review.rating)
        total += rating
    }
    let popcornRating = total/reviews.length;
    let popcornRatingrd = Math.floor(popcornRating).toFixed(2);
    const popcorns = [];

    for(let i=1; i<=5; i++) {
        if(i <= popcornRatingrd) {
            popcorns.push(<GiPopcorn
            size={30}
            color= {"#ffc107"}
            />)
        } else {
        popcorns.push(<GiPopcorn
            size={30}
            color= {"#e4e5e9"}
            />)
        }
    }

    return(
        <>
        <div className="container p-0 m-0">
            <p className="my-0">Popcorn rating:</p>
                <div className="row d-flex justify-content-center align-items-center">
                        {popcorns}
                    <p className="m-2 ">({popcornRating ? popcornRating  : "no reviews yet"})</p>
                </div>
        </div>
        </>
    );
}

export default PopcornRating;
