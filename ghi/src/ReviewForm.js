import { useEffect, useState } from 'react';
import { useAuthContext } from "@galvanize-inc/jwtdown-for-react";
import { useParams } from 'react-router-dom';
import { GiPopcorn } from "react-icons/gi";
import { useNavigate } from "react-router-dom";
import { format } from 'date-fns';


function ReviewMovie() {
    const { token } = useAuthContext();
    const params = useParams();
    const movie_id = params.movie_id;
    const navigate = useNavigate();

    const [movie, setMovie] = useState();
    const [title, setTitle] = useState("");
    const [rating, setRating] = useState(null)
    const [review, setReview] = useState("")
    const [hover, setHover] = useState(null);
    const [error, setError] = useState(null);


    const handleSubmit = async (e) => {
        e.preventDefault();

        const data = {
            rating,
            title,
            review,
            date_added: new Date().toISOString().split('T')[0],
            movie_id: Number(movie_id)
        };

       try {
        const response = await fetch(`${process.env.REACT_APP_API_HOST}/api/reviews`, {
            method: 'POST',
            credentials: "include",
            headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`
            },
            body: JSON.stringify(data)
        });

        if (!response.ok) {
            throw new Error('Failed to submit review');
        }

        setTitle("");
        setRating(null);
        setReview("");
        navigate(`/movies/${movie_id}`);
        } catch (error) {
        setError(error.message);
        }
    };

    const handleTitleChange = (e) => {
        const value = e.target.value;
        setTitle(value);
    }

    const handleReviewChange = (e) => {
        const value = e.target.value;
        setReview(value);
    }


    useEffect(() => {
        const getMovie = async () => {
        try {
            const response = await fetch(`${process.env.REACT_APP_API_HOST}/api/movies/${movie_id}`, {
            headers: { Authorization: `Bearer ${token}` }
            });

            if (!response.ok) {
            throw new Error('Failed to fetch movie data');
            }

            const data = await response.json();
            setMovie(data);
        } catch (error) {
            setError(error.message);
        }
        };

        getMovie();
    }, [movie_id, token]);

    if (error) {
        return <p>Error: {error}</p>;
    }

    if(movie) {
        return (
        <div class="container">
            <div class="row">
                <div class="col col-lg-4 py-5">
                    <div class="card mb-3">
                        <img class="card-img-top" src={movie.picture} alt={movie.title} />
                        <div class="card-body">
                            <h5 class="card-title">{movie.title}</h5>
                                <h6 class="card-subtitle mb-2 text-body-secondary">{ format(movie.release_date, 'MMMM d, yyyy')}</h6>
                            <p class="card-text">{movie.description}</p>
                            <p class="card-text"><small class="text-muted"></small></p>
                        </div>
                    </div>
                </div>
                <div class="col py-5">
                    <form onSubmit={handleSubmit}>
                        <div class="container d-flex p-0 align-items-center">
                            <label for="exampleFormControlInput1">Rating:</label>
                            {/* popcorn ratings element */}
                            <div class="container d-flex">
                                {[...Array(5)].map((star, index) => {
                                    const currentRating = index + 1;
                                    return(
                                        <label>
                                            <input
                                                type="radio"
                                                name="rating"
                                                value={currentRating}
                                                onClick= {() => setRating(currentRating.toString())}
                                            />
                                                <GiPopcorn
                                                    class="star"
                                                    size= {30}
                                                    color= {currentRating <= (hover || rating) ? "#ffc107" : "#e4e5e9"}
                                                    onMouseEnter={() => setHover(currentRating)}
                                                    onMouseLeave={() => setHover(null)}
                                                />
                                        </label>
                                    )
                                })}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput1">Title:</label>
                                <input
                                    onChange={handleTitleChange}
                                    value={title} type="text"
                                    class="form-control" id="title"
                                    placeholder='e.g. "I loved this movie!"'
                                />
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlTextarea1">Review:</label>
                                <textarea
                                    onChange={handleReviewChange}
                                    value={review}
                                    class="form-control"
                                    id="exampleFormControlTextarea1"
                                    rows="3" placeholder='e.g. "I loved this movie because..."'>

                                </textarea>
                        </div>
                        <button type="submit" class="btn btn-primary mb-2">Submit Review</button>
                    </form>
                </div>
            </div>
        </div>
        )
    }
}

export default ReviewMovie;
