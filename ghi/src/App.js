import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Nav from './Nav';
import Dashboard from './Dashboard';
import SignupForm from "./SignupForm";
import SigninForm from "./SigninForm";
import MovieForm from './MovieForm';
import { AuthProvider } from "@galvanize-inc/jwtdown-for-react";
import Footer from "./Footer";
import MovieDetails from './MovieDetails';
import MoviesList from './MoviesList';
import CommentForm from './CommentForm';
import ReviewMovie from './ReviewForm';
import UsersList from './UsersList';
import Profile from './Profile';
import EditMovie from './EditMovie';
import EditProfileForm from './EditProfileForm';

function App() {
  const domain = /https:\/\/[^/]+/;
  const basename = process.env.PUBLIC_URL.replace(domain, '')
  return (
    <BrowserRouter basename={basename}>
      <AuthProvider baseUrl={`${process.env.REACT_APP_API_HOST}`}>
        <Nav />
          <div className="container">
            <Routes>
              <Route path="/" element={<Dashboard />} />
              <Route path="/signup" element={<SignupForm />} />
              <Route path="/signin" element={<SigninForm />} />
              <Route path="/movies">
                <Route path="new" element={<MovieForm />} />
                <Route path="/movies" element={<MoviesList />} />
              </Route>
              <Route path="/movies/:movie_id" element={<MovieDetails />} />
              <Route path="/movies/:movie_id/edit" element={<EditMovie />} />
              <Route path="/movies/:movie_id/review" element={<ReviewMovie />} />
              <Route path="/users"  element={<UsersList />} />
              <Route path="/users/:username"  element={<Profile />} />
              <Route path="/users/:username/edit"  element={<EditProfileForm />} />
              <Route path="/reviews/:review_id/comments/new" element={<CommentForm />} />
            </Routes>
          </div>
        <Footer />
      </AuthProvider>
    </BrowserRouter>
  );
}

export default App;
