import { NavLink, useNavigate } from 'react-router-dom';
import ProfilePic from './ProfilePic';
import { useEffect } from "react";
import { useAuthContext} from "@galvanize-inc/jwtdown-for-react";
import logo from "./images/pp_no_back.png";



function Nav() {
    const navigate = useNavigate();
    const { token } = useAuthContext();

    useEffect(() => {
        if(!token) {
        navigate("/signin");
        // eslint-disable-next-line react-hooks/exhaustive-deps
        }}, [token])


    return(
        <>
    <nav className="navbar navbar-expand-lg navbar-light bg-light px-5">
        <div className="container-fluid" style={{ height: '95px' }}>
            <NavLink className="navbar-brand" to="/">
                <img src={logo} width="115" height="115" alt="popcorn picks logo" />
            </NavLink>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse space-between mx-2" id="navbarSupportedContent">
            <ul className="navbar-nav ">
            {token && (
                <>
                    <li className="nav-item mx-5">
                        <NavLink className="nav-link" to="/movies" style={{ fontSize: '1.1rem' }}>Movies</NavLink>
                    </li>
                    <li className="nav-item mx-5">
                        <NavLink className="nav-link" to="/users" style={{ fontSize: '1.1rem' }}>Users</NavLink>
                    </li>
                </>
            )}
            </ul>
            {!token && (
            <ul className="navbar-nav p-1">
                {!token && (
                <>
                <li className="nav-item mx-5">
                    <NavLink className="nav-link" to="/signin" style={{ fontSize: '1.1rem' }}>Sign in</NavLink>
                </li>
                <li className="nav-item mx-5">
                    <NavLink className="nav-link" to="/signup" style={{ fontSize: '1.1rem' }}>Sign up</NavLink>
                </li>
                </>
                )}
            </ul>
            )}
        </div>
            {token && (
            <ProfilePic />
            )}
        </div>
    </nav>
    </>
    );
}

export default Nav
