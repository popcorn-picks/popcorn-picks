import React, { useEffect, useState } from 'react';
import { NavLink, useNavigate } from 'react-router-dom';
import useToken, { useAuthContext } from "@galvanize-inc/jwtdown-for-react";

const profile_pic_style = {
  width: "80px",
  height: '80px'
};

function ProfilePic() {
  const [profilePicture, setProfilePicture] = useState(null);
  const [username, setUsername] = useState(null);
  const { token } = useAuthContext();
  const navigate = useNavigate();
  const { logout } = useToken();
  const [error, setError] = useState(null);

  const signOut = async () => {
    try {
      logout();
      navigate("/signin");
    } catch (error) {
      setError("Failed to sign out");
    }
  };

  const getToken = async () => {
    try {
      const response = await fetch(`${process.env.REACT_APP_API_HOST}/token`, {
        credentials: "include"
      });

      if (!response.ok) {
        throw new Error('Failed to fetch token data');
      }

      const data = await response.json();
      const account = data["account"];
      setUsername(account.username);
      setProfilePicture(account.profile_pic);

    } catch (error) {
      setError("Failed to fetch profile data");
    }
  };

  useEffect(() => {
    getToken();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [token]);

  return (
    <div className="dropdown">
      <NavLink className="dropdown-toggle" id="profileDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <img className="rounded-circle shadow-4-strong" style={profile_pic_style} alt={username} src={profilePicture} />
      </NavLink>
      <ul className="dropdown-menu" aria-labelledby="profileDropdown">
        <li><NavLink className="dropdown-item" to={`/users/${username}`} >View Profile</NavLink></li>
        <li><hr className="dropdown-divider" /></li>
        <li><NavLink className="dropdown-item" onClick={() => signOut()}>Logout</NavLink></li>
      </ul>
      {error && <p>Error: {error}</p>}
    </div>
  );
}

export default ProfilePic;