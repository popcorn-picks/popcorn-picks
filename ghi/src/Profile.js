import { useState, useEffect } from 'react';
import { useAuthContext } from "@galvanize-inc/jwtdown-for-react";
import {
  MDBCard,
  MDBCardBody,
  MDBBtn,
  MDBCol,
  MDBRow,
}
from 'mdb-react-ui-kit';
import { useParams, Link } from 'react-router-dom';
import { format } from 'date-fns';
import { GiPopcorn } from "react-icons/gi";
import { FaTrashAlt } from "react-icons/fa";
import UserMovies from './UserMovies';

const profile_pic_style = {
  width: "100px",
  height: '100px'
};


function Profile() {
    const [user, setUser] =useState();
    const [reviews, setReviews] = useState();
    const [movies, setMovies] = useState();
    const [error, setError] = useState(null);
    const { token } = useAuthContext();
    const params = useParams();
    const username = params.username;
    const [account, setAccount] = useState();
    let isUser = false;

    const userReviews = [];

    if(reviews && user) {
        for (const review of reviews) {
            if (user.id === review.user_id) {
                userReviews.push(review)
            }
        }
    }

    function getMovieTitle(movie_id) {
        for (const movie of movies ) {
            if (movie.movie_id === movie_id) {
                return movie.title;
            }
        }
    }

    function getPopcorns(rating) {
        const userPopcornRating = [];
        for(let i=0; i<=4; i++) {
            if(i < rating) {
                userPopcornRating.push(<GiPopcorn
                    size={30}
                    color= {"#ffc107"}
                />)
            } else {
                userPopcornRating.push(<GiPopcorn
                    size={30}
                    color= {"#e4e5e9"}
                />)
        }
        }
        return userPopcornRating;
    }

  useEffect(() => {
    const getUser = async () => {
      try {
        const response = await fetch(`${process.env.REACT_APP_API_HOST}/api/users/${username}`, {
          headers: { Authorization: `Bearer ${token}` }
        });

        if (!response.ok) {
          throw new Error('Failed to fetch user data');
        }

        const data = await response.json();
        setUser(data);
      } catch (error) {
        setError(error.message);
      }
    };

    getUser();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [token, params]);


  const getToken = async() => {
        try {
            const response = await fetch(`${process.env.REACT_APP_API_HOST}/token`, {
                credentials: "include"
            });
            if (!response.ok) {
                throw new Error('Failed to fetch token data');
            }
            const data = await response.json();
            const account = data["account"];
            setAccount(account)
            // eslint-disable-next-line eqeqeq
        } catch (error) {
            console.log(error)
        }
    };

  useEffect(() => {
    getToken();
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  useEffect(() => {
    const getReviews = async () => {
      try {
        const response = await fetch(`${process.env.REACT_APP_API_HOST}/api/reviews/`, {
          headers: { Authorization: `Bearer ${token}` }
        });

        if (!response.ok) {
          throw new Error('Failed to fetch reviews');
        }

        const data = await response.json();
        setReviews(data.reviews);
      } catch (error) {
        setError(error.message);
      }
    };

    getReviews();
  }, [token, movies]);

useEffect(() => {
    const getMovies = async () => {
      try {
        const response = await fetch(`${process.env.REACT_APP_API_HOST}/api/movies/`, {
          headers: { Authorization: `Bearer ${token}` }
        });

        if (!response.ok) {
          throw new Error('Failed to fetch movies');
        }

        const data = await response.json();
        setMovies(data.movies);
      } catch (error) {
        setError(error.message);
      }
    };

    getMovies();
  }, [token]);

  if(user && account) {
    // eslint-disable-next-line eqeqeq
    if(user.id == account.id) {
      isUser = true
    }
  }


    if(user && reviews && movies) {
        return(
            <>
            <MDBRow>
                <MDBCol lg="4" className="py-5">
                    <MDBCard className="mb-4 sticky-top">
                        <MDBCardBody className="text-center">
                            <img className="rounded-circle shadow-4-strong m-3" style={profile_pic_style} alt={username} src={user.profile_pic} />
                            <h5 className="mb-1">{user.first_name} {user.last_name}</h5>
                            <p className="text-muted mb-4"><strong>@{user.username}</strong></p>
                            <p>
                              {isUser && (
                                <button className="btn btn-info" data-toggle="button">
                                  <Link className="btn btn-info" to={`/users/${username}/edit`} >Edit Profile</Link>
                                </button>
                              )}
                            </p>
                            <div className="d-flex justify-content-center mb-2">
                            <MDBBtn>Follow</MDBBtn>
                            <MDBBtn outline className="ms-1 ml-2">Message</MDBBtn>
                            </div>
                        </MDBCardBody>
                        </MDBCard>
                </MDBCol>
                {/* user's reviews */}
                <MDBCol lg="8" className="py-5">
                  <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item" role="presentation">
                      <button class="nav-link active" id="reviews-tab" data-bs-toggle="tab" data-bs-target="#reviews-tab-pane" type="button" role="tab" aria-controls="reviews-tab-pane" aria-selected="true">Reviews</button>
                    </li>
                    <li class="nav-item" role="presentation">
                      <button class="nav-link" id="movies-tab" data-bs-toggle="tab" data-bs-target="#movies-tab-pane" type="button" role="tab" aria-controls="movies-tab-pane" aria-selected="false">Movies</button>
                    </li>
                    <li class="nav-item" role="presentation">
                      <button class="nav-link" id="friends-tab" data-bs-toggle="tab" data-bs-target="#friends-tab-pane" type="button" role="tab" aria-controls="friends-tab-pane" aria-selected="false">Friends</button>
                    </li>
                    <li class="nav-item" role="presentation">
                      <button class="nav-link" id="forums-tab" data-bs-toggle="tab" data-bs-target="#forums-tab-pane" type="button" role="tab" aria-controls="forums-tab-pane" aria-selected="false">Forums</button>
                    </li>
                  </ul>
                  <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="reviews-tab-pane" role="tabpanel" aria-labelledby="reviews-tab" tabindex="0">
                        {
                          userReviews.map((review) => {
                          // eslint-disable-next-line eqeqeq
                          if(review == userReviews[0]) {
                            return (
                              <MDBCard className="mb-4 px-3 border-top-0">
                                    <MDBCardBody>
                                        <MDBRow className="d-flex align-items-center justify-content-between">
                                            <Link to={`/movies/${review.movie_id}`}><h5 class="m-0">{getMovieTitle(review.movie_id)} </h5></Link>
                                            <div>
                                                {getPopcorns(review.rating)}
                                            </div>
                                        </MDBRow>
                                        <MDBRow>
                                            <p class="text-muted m-0">reviewed on {format(review.date_added, 'M-d-yyyy')} </p>
                                        </MDBRow>
                                        <MDBRow className="pt-4">
                                            <h4 class="m-0">{review.title} </h4>
                                        </MDBRow>
                                        <MDBRow>
                                            <p class="m-0">{review.review}</p>
                                        </MDBRow>
                                        <MDBRow className="d-flex justify-content-end">
                                            <FaTrashAlt
                                                size={25}
                                                color={"#FF0000"}
                                            />
                                        </MDBRow>
                                    </MDBCardBody>
                                </MDBCard>
                                )
                              }
                          else {
                            return (
                                <MDBCard className="mb-4 px-3">
                                    <MDBCardBody>
                                        <MDBRow className="d-flex align-items-center justify-content-between">
                                            <Link to={`/movies/${review.movie_id}`}><h5 class="m-0">{getMovieTitle(review.movie_id)} </h5></Link>
                                            <div>
                                                {getPopcorns(review.rating)}
                                            </div>
                                        </MDBRow>
                                        <MDBRow>
                                            <p class="text-muted m-0">reviewed on {format(review.date_added, 'M-d-yyyy')} </p>
                                        </MDBRow>
                                        <MDBRow className="pt-4">
                                            <h4 class="m-0">{review.title} </h4>
                                        </MDBRow>
                                        <MDBRow>
                                            <p class="m-0">{review.review}</p>
                                        </MDBRow>
                                        <MDBRow className="d-flex justify-content-end">
                                            <FaTrashAlt
                                                size={25}
                                                color={"#FF0000"}
                                            />
                                        </MDBRow>
                                    </MDBCardBody>
                                </MDBCard>
                            )}})}
                      </div>
                    <div class="tab-pane fade" id="movies-tab-pane" role="tabpanel" aria-labelledby="movies-tab" tabindex="0">
                      <UserMovies
                        user={user}
                        movies={movies}
                      />
                    </div>
                    <div class="tab-pane fade" id="friends-tab-pane" role="tabpanel" aria-labelledby="friends-tab" tabindex="0">
                      <MDBCard className="mb-4 p-5 border-top-0 text-center">
                        <h1>under construction!</h1>
                      </MDBCard>
                    </div>
                    <div class="tab-pane fade" id="forums-tab-pane" role="tabpanel" aria-labelledby="forums-tab" tabindex="0">
                      <MDBCard className="mb-4 p-5 border-top-0 text-center">
                        <h1>under construction!</h1>
                      </MDBCard>
                    </div>
                    </div>
                </MDBCol>
            </MDBRow>
          </>
        )
    } else if (error) {
        return <p>Error: {error}</p>;
    }

    }

export default Profile;
