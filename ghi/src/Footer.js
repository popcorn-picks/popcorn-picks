import { FaSquareGitlab } from "react-icons/fa6";
import { Link} from 'react-router-dom';


const footer = {
    backgroundColor: "rgba(0, 0, 0, 0.05)"
}

function Footer() {
    return (
    <footer className="footer d-flex bg-body-tertiary text-center text-lg-start align-content-end align-items-center mt-auto" style={footer}>
        <div className="container">
            <div className="text-center p-3">
                © 2024 <Link className="text-body" to="https://gitlab.com/popcorn-picks/popcorn-picks">PopcornPicks.com</Link>
            </div>
        </div>
        <FaSquareGitlab
                size={30}
            />
    </footer>
    );
}

export default Footer
