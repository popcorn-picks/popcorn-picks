import React, { useState, useEffect } from 'react';
import { useNavigate, Link } from "react-router-dom";
import { useAuthContext } from "@galvanize-inc/jwtdown-for-react";


// warning for missing fields
function MissingField(props) {
    const { missingField } = props
    if(missingField) {
    return(
        <div className="alert alert-danger mt-3" role="alert">
            Please fill out all missing fields.
        </div>
        )
    }
}

// warning for duplicate movies
function MovieExists(props) {
    const { movieExists, title } = props;
    if (movieExists) {
        return (
            <div class="alert alert-warning mt-3" role="alert">
                "{title}" has already been PopcornPicked!
                <Link to="/movies" > Check it out!</Link>
            </div>
        )
    }
}

function MovieForm() {
    const [ title, setTitle ] = useState("");
    const [ picture, setPicture ] = useState("");
    const [ description, setDescription ] = useState("");
    const [ releaseDate, setReleaseDate ] = useState("");
    const [ rating, setRating ] = useState("");
    const [ director, setDirector ] = useState("");
    const [missingField, setMissingField] = useState(false);
    const [existingMovies, setExistingMovies] = useState([]);
    const [movieExists, setMovieExists] = useState(false);
    const token = useAuthContext();
    const navigate = useNavigate();
    const [form, setForm] = useState(false)

    let movie_id = 0;

    const getMovies = async() => {
        try {
            const movies_response = await fetch(`${process.env.REACT_APP_API_HOST}/api/movies`, {
                headers: { Authorization: `Bearer ${token}` }
            });
            if(movies_response.ok) {
                const data = await movies_response.json();
                let movies=[]
                for (const movie of data.movies) {
                    movies.push(movie["title"].toLowerCase())
                setExistingMovies(movies)
                }
            }
        } catch (error) {
            console.log("could not retrieve movies. Error:", error)
        }
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
    useEffect(() => {getMovies()}, [token, form])

    const handleSubmit = async (e) => {
        e.preventDefault();

        const movie_data = {
            title,
            picture,
            description,
            release_date: releaseDate,
            rating,
            upload_date: new Date().toISOString().split('T')[0],
        };


        if (existingMovies.includes(title.toLowerCase())){
            setMovieExists(true)
        } else {
            try {
                const movie_response = await fetch(`${process.env.REACT_APP_API_HOST}/api/movies`, {
                        method: 'POST',
                        credentials: "include",
                        headers: {
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify(movie_data),
                });

                if (movie_response.ok) {
                    const newMovie = await movie_response.json();

                    setTitle("");
                    setPicture("");
                    setDescription("");
                    setReleaseDate("");
                    setRating("");
                    movie_id = newMovie.id
                    setMissingField(false)

                }

                const director_data = {
                    director_name: director,
                    movie_id: movie_id
                }

                navigate(`/movies/${movie_id}`)

                const director_response = await fetch(`${process.env.REACT_APP_API_HOST}/api/directors`, {
                    method: 'POST',
                    credentials: "include",
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(director_data),
                });

                if (director_response.ok) {
                    setDirector("")
                }
            } catch (error) {
                console.log("there was an error", error)
                setMissingField(!missingField)
            }
        }
    }

    const handleTitleChange = (e) => {
        const value = e.target.value;
        setTitle(value);
    };

    const handlePictureChange = (e) => {
        const value = e.target.value;
        setPicture(value);
    };

    const handleDescriptionChange = (e) => {
        const value = e.target.value;
        setDescription(value);
    };

    const handleReleaseDateChange = (e) => {
        const value = e.target.value;
        setReleaseDate(value);
    };

    const handleRatingChange = (e) => {
        const value = e.target.value;
        setRating(value);
    };

    const handleDirectorChange = (e) => {
        const value = e.target.value;
        setDirector(value)
    }

    // this will trigger that the form is being filled out and make the api call to movies
    const fillingForm = (e) => {
        setForm(true)
    }

    return (
        <div className="form-body">
        <div className="form-holder">
            <div className="form-content">
                <div className="form-items">
                    <h1>Create a Movie</h1>
                    <form onSubmit={handleSubmit} id="create-movie-form">
                        <div className="col-md-12">
                            <input
                                placeholder="Title"
                                onChange={handleTitleChange}
                                onClick={fillingForm}
                                value={title}
                                required
                                type="text"
                                name="title"
                                id="title"
                                className="form-control"
                            />
                        </div>
                        <div className="col-md-12">
                            <input
                                placeholder="Picture URL"
                                onChange={handlePictureChange}
                                value={picture}
                                required
                                type="text"
                                name="picture"
                                id="picture"
                                className="form-control"
                            />
                        </div>
                        <div className="col-md-12">
                            <textarea
                                placeholder="Description"
                                className="form-control"
                                onChange={handleDescriptionChange}
                                value={description}
                                required
                                type="text"
                                name="description"
                                id="description"
                            >
                            </textarea>
                        </div>
                        <div className="col-md-12">
                            <label className="form-label">Release Date:</label>
                            <input
                                style={{ background: '#e1e1e1'}}
                                onChange={handleReleaseDateChange}
                                value={releaseDate}
                                required
                                type="date"
                                name="release_date"
                                id="release_date"
                                className="form-control"
                            />
                        </div>
                        <div className="col-md-12">
                            <select
                                onChange={handleRatingChange}
                                value={rating}
                                required
                                name="rating"
                                id="rating"
                                className="form-select"
                            >
                                <option selected>Select rating</option>
                                <option value="G">G</option>
                                <option value="PG">PG</option>
                                <option value="PG-13">PG-13</option>
                                <option value="R">R</option>
                                <option value="NC-17">NC-17</option>
                            </select>
                        </div>
                        <div className="col-md-12">
                            <input
                                placeholder='Director(s)'
                                onChange={handleDirectorChange}
                                value={director}
                                required
                                type="text"
                                name="director_name"
                                id="director_name"
                                className="form-control"
                            />
                        </div>
                        <div>
                            <input
                            className="btn btn-primary" style={{ margin: '20px 10px 10px 15px' }}
                            type="submit"
                            value="Create"
                            onClick={handleSubmit}
                            />
                        </div>
                    </form>
                    <MissingField
                        missingField= {missingField}
                    />
                    <MovieExists
                        movieExists = {movieExists}
                        title = {title}
                    />
                </div>
            </div>
        </div>
    </div>
    );
}


export default MovieForm;
