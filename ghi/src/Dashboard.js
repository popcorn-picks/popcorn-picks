import { useEffect, useState } from 'react';
import { NavLink, Link} from 'react-router-dom';
import { useAuthContext } from "@galvanize-inc/jwtdown-for-react";


function GetStarted() {
    const token = useAuthContext();
    if (token.token) {
        return (
            <Link className="btn btn-danger btn-lg" to="/movies" role="button">Get Started!</Link>
        )
    } else {
        return (
            <Link className="btn btn-danger btn-lg" to="/signup" role="button">Get Started!</Link>
        )
    }
}


function Dashboard() {
    const[movies, setMovies] = useState([])


    const getMovies = async () => {
        const response = await fetch(`${process.env.REACT_APP_API_HOST}/api/movies/`);
        if (response.ok) {
            const data = await response.json();
            setMovies(data.movies);
        }
    }
    useEffect(() => {getMovies()}, []);

    return (
        <>
        <h1 className="display-5 py-5" >Today's <strong>hottest</strong> movies</h1>
        <div className="container">
                <div className="row flex-nowrap overflow-auto">
                    {movies.map(movie => {
                        return (
                            <div className="col-4" key={movie.movie_id}>
                                <NavLink to={`/movies/${movie.movie_id}`} key={movie.movie_id}>
                                    <img src={movie.picture} className="card-img-top movie-card" alt={movie.title} />
                                    <h5 className="card-title text-center p-3" >{movie.title}</h5>
                                </NavLink>
                            </div>
                        );
                    })}
                </div>

        </div>
        <div className="jumbotron my-5 p-5">
            <h1 className="display-4">One place for all your PopcornPicks</h1>
            <p className="lead">
                Popcorn Picks is the go-to platform for movie enthusiasts to share and discover their favorite films.
                Tailor your movie-watching experience by creating an account, listing your favorite movies, writing reviews, and engaging in discussions through comments.
                With a sleek design and functionalities, Popcorn Picks is more than just a movie rating website – it's your personalized <strong>cinematic journey.</strong>
            </p>
            <hr className="my-4" />
            <p>Explore our wide selection of movies!</p>
            <p className="lead">
                <GetStarted />
            </p>
        </div>
        </>
    )
}

export default Dashboard;
