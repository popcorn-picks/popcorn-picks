import { useEffect, useState } from 'react';
import { NavLink } from 'react-router-dom';
import { useAuthContext } from "@galvanize-inc/jwtdown-for-react";
import { useNavigate } from "react-router-dom";
import { format } from 'date-fns';


function MoviesList() {
    const [movies, setMovies] = useState([]);
    const [filterMovies, setFilterMovies] = useState([]);
    const [hoveredMovie, setHoveredMovie] = useState(null);
    const { token } = useAuthContext();
    const navigate = useNavigate();
    const [error, setError] = useState(null);

    const handleFilter = (event) => {
        const searchTitle = event.target.value;
        setFilterMovies(searchTitle);
    };

    useEffect(() => {
        const getData = async () => {
            try {
                const response = await fetch(`${process.env.REACT_APP_API_HOST}/api/movies/`, {
                    headers: { Authorization: `Bearer ${token}` }
                });

                if (!response.ok) {
                    throw new Error('Failed to fetch movies');
                }

                const data = await response.json();
                setMovies(data.movies);
            } catch (error) {
                setError('Error fetching movies. Please try again.');
                console.error('Error fetching movies:', error.message);
            }
        };

        getData();
    }, [token]);

    let filter = movies;
    if (filterMovies.length > 0) {
        filter = movies.filter(movie =>
            movie.title.toUpperCase().includes(filterMovies.toUpperCase())
        );
    }

    const movieCardStyle = {
    base: {
        transition: 'transform 0.2s ease',
    },
    hover: {
        transform: 'scale(1.1)',
    },
};

    return (
        <>
            <div className='container my-5'>
                <div>
                    <h1>Movie List</h1>
                </div>
                    <button type="button" className="btn btn-outline-danger" style={{ marginLeft: '793px', marginBottom: '10px' }} onClick={() => navigate(`/movies/new`)}> Add movie</button>
                <div className='input-group mb-3' style={{ maxWidth: '905px' }}>
                    <input
                        type="text"
                        className="form-control"
                        aria-label="title"
                        aria-describedby='button-addon2'
                        onChange={handleFilter}
                        placeholder='e.g. "Forest Gump"'
                        value={filterMovies}
                        data={movies}
                    />
                </div>
                {error && <p className="text-danger">{error}</p>}
                <div className="container">
                    <div className="row">
                        {filter.map(movie => {
                            return (
                                <div 
                                key={movie.movie_id}
                                className="card mb-3 shadow col-6 col-sm-3 m-3 p-3"
                                style={hoveredMovie === movie.movie_id ? movieCardStyle.hover : movieCardStyle.base}
                                onMouseEnter={() => setHoveredMovie(movie.movie_id)}
                                onMouseLeave={() => setHoveredMovie(null)}
                                >
                                    <NavLink to={`/movies/${movie.movie_id}`} className="text-center">
                                        {/* link should go to details*/}
                                        <img src={movie.picture} className="card-img-top" alt={movie.title} />
                                        <h5 className="card-title text-wrap pt-3 mb-0">{movie.title}</h5>
                                        <h6 className="mt-0 pt-1">({ format(movie.release_date, 'yyyy') })</h6>
                                    </NavLink>
                                </div>
                            );
                        })}
                    </div>
                </div>
            </div>
        </>
    );
}

export default MoviesList;
