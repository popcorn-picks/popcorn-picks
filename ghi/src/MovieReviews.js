import { GiPopcorn } from "react-icons/gi";
import { format } from 'date-fns';
import { MdAddComment } from "react-icons/md";
import CommentForm from "./CommentForm";


const profile_pic_style = {

    width: "50px",
    height: "50px"

}

function MovieReviews(props) {
    const { reviews, users } = props


    function getUsername(user_id) {
        for(let user of users) {
            if (user_id === user.id) {
                return user.username
            }
        }
    }

    function getProfilePic(user_id) {
        for (let user of users) {
            if (user_id === user.id) {
                return user.profile_picture
            }
        }
    }

    function getPopcorns(rating) {
        const userPopcornRating = [];
        for(let i=0; i<=4; i++) {
            if(i < rating) {
                userPopcornRating.push(<GiPopcorn
                    size={30}
                    color={"#ffc107"}
                />)
            } else {
                userPopcornRating.push(<GiPopcorn
                    size={30}
                    color={"#e4e5e9"}
                />)
        }
        }
        return userPopcornRating;
    }


    return(
        <div>
            {reviews.map((review) => {
                return (
                    <div className="card mb-3 w-100">
                        <div className="container row d-flex align-items-center">
                            <img className="rounded-circle shadow-4-strong m-3" style={profile_pic_style} key={review.id} alt={getUsername(review.user_id)} src={getProfilePic(review.user_id)}/>
                                <p className="text-muted m-0">{getUsername(review.user_id)} | {format(review.date_added, 'M-d-yyyy')} </p>
                        </div>
                        <div className="card-body pt-0">
                            <div className="container row d-flex justify-content-between mb-2">
                                <h5 className="card-title m-0 d-flex align-items-center" key={review.id}>{review.title}</h5>
                                <div>
                                    {getPopcorns(review.rating)}
                                </div>
                            </div>
                            <p className="card-text m-0">{review.review}</p>

                        <div className="container row d-flex align-items-center justify-content-end">
                        {/* the 10 is just a placeholder for now until we have comments component */}
                            <small className="m-0 mr-2">
                                comments
                            </small>
                            <button className="btn" type="button" data-toggle="collapse" data-target={`#add-comment${review.id}`} aria-expanded="false" aria-controls={`add-comment${review.id}`}>
                                <MdAddComment
                                size= {25}
                                />
                            </button>
                        </div>
                            <div className="collapse" id={`add-comment${review.id}`}>
                                <CommentForm
                                    review_id = {review.id}
                                />
                            </div>
                        </div>
                    </div>
                )
            })}
        </div>
    )
}

export default MovieReviews;
