from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
import os
from authenticator import authenticator
from routers import (
    users, movies, reviews, cast,
    directors, comments, review_comments, friends
)

app = FastAPI()
app.include_router(comments.router)


app.include_router(users.router)
app.include_router(movies.router)
app.include_router(reviews.router)
app.include_router(authenticator.router)
app.include_router(cast.router)
app.include_router(directors.router)
app.include_router(review_comments.router)
app.include_router(friends.router)


app.add_middleware(
    CORSMiddleware,
    allow_origins=[
        os.environ.get("CORS_HOST", "http://localhost:3000")
    ],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/")
def root():
    return {"message": "You hit the root path!"}


@app.get("/api/launch-details")
def launch_details():
    return {
        "launch_details": {
            "module": 3,
            "week": 17,
            "day": 5,
            "hour": 19,
            "min": "00"
        }
    }
