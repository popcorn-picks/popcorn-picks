from fastapi.testclient import TestClient
from authenticator import authenticator
from main import app
from queries.reviews import ReviewRepo

client = TestClient(app)


class EmptyReviewRepo():
    def get_all_reviews(self):
        return {
            "reviews": [
                {
                    "id": 1,
                    "title": "test",
                    "review": "test",
                    "rating": "3",
                    "date_added": "2024-01-29",
                    "user_id": 1,
                    "movie_id": 1
                }
            ]
        }


def fake_get_current_account_data():
    return {
        "id": 1,
        "first_name": "First",
        "last_name": "Last",
        "username": "Hello",
        "profile_pic": "Pic",
        "date_joined": "2024-01-29",
        "active": True
    }


def test_get_all_reviews():
    # Arrange
    app.dependency_overrides[
        authenticator.get_current_account_data
        ] = fake_get_current_account_data
    app.dependency_overrides[ReviewRepo] = EmptyReviewRepo
    expected = {
        "reviews": [
                {
                    "id": 1,
                    "title": "test",
                    "review": "test",
                    "rating": "3",
                    "date_added": "2024-01-29",
                    "user_id": 1,
                    "movie_id": 1
                }
            ]
        }

    # Act
    response = client.get("/api/reviews")
    app.dependency_overrides = {}

    # Assert
    assert response.status_code == 200
    assert response.json() == expected
