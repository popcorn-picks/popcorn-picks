from fastapi.testclient import TestClient
from pydantic import BaseModel
from datetime import date
from queries.users import UserRepo
from main import app
from authenticator import authenticator

client = TestClient(app)


class UserIn(BaseModel):
    first_name: str
    last_name: str
    username: str
    profile_pic: str
    date_joined: date
    password: str
    active: bool


class GetUserRepo:
    def get_user(self, username_to_get):
        return {
            "id": 1,
            "first_name": "First",
            "last_name": "Last",
            "username": "Hello",
            "profile_pic": "Pic",
            "date_joined": "2024-01-29",
            "active": True
        }


def fake_get_current_account_data():

    return {
        "id": 1,
        "first_name": "First",
        "last_name": "Last",
        "username": "Hello",
        "profile_pic": "Pic",
        "date_joined": "2024-01-29",
        "active": True
    }


def test_get_user():
    # Arrange
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data
    app.dependency_overrides[UserRepo] = GetUserRepo
    username_to_get = "Hello"
    expected = {
        "id": 1,
        "first_name": "First",
        "last_name": "Last",
        "username": "Hello",
        "profile_pic": "Pic",
        "date_joined": "2024-01-29",
        "active": True
    }

    # Act
    response = client.get(f"/api/users/{username_to_get}")
    app.dependency_overrides = {}

    # Assert
    assert response.status_code == 200
    assert response.json() == expected
