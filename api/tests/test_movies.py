from fastapi.testclient import TestClient
from authenticator import authenticator
from main import app
from queries.movies import MovieRepo

client = TestClient(app)


class EmptyMovieRepo():
    def get_all_movies(self):
        return {
            "movies": [
                {
                    "id": 1,
                    "title": "test",
                    "description": "An example movie description.",
                    "release_date": "2023-12-25",
                    "rating": "PG",
                    "duration": "120 minutes",
                    "genre": ["Adventure", "Fantasy"],
                    "director": "Director Name"
                }
            ]
        }


def fake_get_current_account_data():
    return {
        "id": 1,
        "first_name": "First",
        "last_name": "Last",
        "username": "Hello",
        "profile_pic": "Pic",
        "date_joined": "2024-01-29",
        "active": True
    }


def test_get_all_movies():
    # Arrange
    app.dependency_overrides[
        authenticator.get_current_account_data
        ] = fake_get_current_account_data
    app.dependency_overrides[MovieRepo] = EmptyMovieRepo
    expected = {
        "movies": [
                {
                    "id": 1,
                    "title": "test",
                    "description": "An example movie description.",
                    "release_date": "2023-12-25",
                    "rating": "PG",
                    "duration": "120 minutes",
                    "genre": ["Adventure", "Fantasy"],
                    "director": "Director Name"
                }
            ]
        }

    # Act
    response = client.get("/api/movies")
    app.dependency_overrides = {}

    # Assert
    assert response.status_code == 200
    assert response.json() == expected
