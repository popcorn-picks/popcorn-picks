from fastapi.testclient import TestClient
from queries.comments import CommentRepo
from main import app
from authenticator import authenticator


client = TestClient(app)


class GetCommentRepo():
    def get_all_comments(self):
        return {
            "comments": [
                {
                    "id": 1,
                    "comment": "test",
                    "user_id": 1,
                    "date_time": "2024-01-29",
                    "review_id": 1
                }
            ]
        }


def fake_get_current_account_data():
    return {
        "id": 1,
        "first_name": "First",
        "last_name": "Last",
        "username": "Hello",
        "profile_pic": "Pic",
        "date_joined": "2024-01-29",
        "active": True
    }


def test_get_all_comments():
    # Arrange
    app.dependency_overrides[
        authenticator.get_current_account_data
        ] = fake_get_current_account_data
    app.dependency_overrides[CommentRepo] = GetCommentRepo
    expected = {
            "comments": [
                {
                    "id": 1,
                    "comment": "test",
                    "user_id": 1,
                    "date_time": "2024-01-29",
                    "review_id": 1
                }
            ]
        }

    # Act
    response = client.get("/api/comments")
    app.dependency_overrides = {}

    # Assert
    assert response.status_code == 200
    assert response.json() == expected
