from fastapi import APIRouter, Depends
from queries.comments import CommentIn, CommentOut, CommentRepo
from authenticator import authenticator


router = APIRouter()


@router.post("/api/comments", response_model=CommentOut)
def create_comment(
    comment: CommentIn,
    repo: CommentRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data)
):
    user_id = account_data["id"]
    return repo.create_comment(user_id, comment)
    # try:
    #     print(account_data)
    #     return repo.create_comment(comment)
    # except Exception as e:
    #     print(e)


@router.get("/api/comments", response_model=dict)
def get_all_comments(
    repo: CommentRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data)
) -> dict:
    return repo.get_all_comments()


@router.put("/comments/{comment_id}", response_model=CommentOut)
def get_comment_details(
    comment_id: int,
    repo: CommentRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
) -> CommentOut:
    return repo.get_comment_details(comment_id)


@router.put("/comments/{comment_id}", response_model=CommentOut)
def update_comment(
    comment_id: int,
    comment: CommentIn,
    repo: CommentRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data)

) -> CommentOut:
    return repo.update_comment(comment_id, comment)


@router.delete("/api/comments/{comment_id}", response_model=dict)
def delete_comment(
    comment_id: int,
    repo: CommentRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data)
) -> dict:
    return repo.delete_comment(comment_id)
