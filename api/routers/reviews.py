from fastapi import (
    APIRouter,
    Request,
    Response,
    Depends,
)
# from pydantic import BaseModel
from queries.reviews import (
    ReviewIn,
    ReviewOut,
    ReviewRepo,
)
# from jwtdown_fastapi.authentication import Token
from authenticator import authenticator

router = APIRouter()


@router.post("/api/reviews", response_model=ReviewOut)
async def create_review(
    review: ReviewIn,
    request: Request,
    response: Response,
    repo: ReviewRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    user_id = account_data["id"]
    return repo.create_review(user_id, review)


@router.get("/api/reviews", response_model=dict)
def get_all_reviews(
    response: Response,
    repo: ReviewRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
) -> dict:
    reviews_data = repo.get_all_reviews()
    return reviews_data


@router.get("/api/reviews/{review_id}", response_model=ReviewOut)
def get_review_details(
    review_id: int,
    repo: ReviewRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
) -> ReviewOut:
    return repo.get_review_details(review_id)


@router.put("/api/reviews/{review_id}", response_model=ReviewOut)
def update_review(
    review_id: int,
    review: ReviewIn,
    repo: ReviewRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
) -> ReviewOut:
    return repo.update_review(review_id, review)


@router.delete("/api/reviews/{review_id}", response_model=dict)
def delete_review(
    review_id: int,
    repo: ReviewRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
) -> dict:
    return repo.delete_review(review_id)
