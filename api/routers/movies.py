from fastapi import APIRouter, Depends, Request, Response
from queries.movies import MovieIn, MovieOut, MovieRepo, MovieOutNoAddedBy
from authenticator import authenticator
router = APIRouter()


@router.post("/api/movies", response_model=MovieOut)
async def create_movie(
    movie: MovieIn,
    request: Request,
    response: Response,
    repo: MovieRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    print("MOVIE", movie)
    user_id = account_data["id"]
    return repo.create_movie(user_id, movie)


@router.get("/api/movies", response_model=dict)
def get_all_movies(
    response: Response,
    repo: MovieRepo = Depends(),
) -> dict:
    movies_data = repo.get_all_movies()
    return movies_data


@router.get("/api/movies/{movie_id}", response_model=MovieOut)
def get_movie_details(
    movie_id: int,
    repo: MovieRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
) -> MovieOut:
    return repo.get_movie_details(movie_id)


@router.put("/api/movies/{movie_id}", response_model=MovieOutNoAddedBy)
def update_movie(
    movie_id: int,
    movie_data: MovieIn,
    repo: MovieRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
) -> MovieOutNoAddedBy:
    return repo.update_movie(movie_id, movie_data)


@router.delete("/api/movies/{movie_id}", response_model=dict)
def delete_movie(
    movie_id: int,
    repo: MovieRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
) -> dict:
    return repo.delete_movie(movie_id)
