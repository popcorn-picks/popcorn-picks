from fastapi import APIRouter, Depends
from authenticator import authenticator
from queries.friends import FriendRepo, Friends

router = APIRouter()


@router.post("/api/users/{friend}", response_model=Friends)
def add_friend(
    user: str,
    friend: str,
    repo: FriendRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data)
) -> Friends:
    return repo.add_friend(user, friend)
