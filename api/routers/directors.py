from fastapi import APIRouter, Depends
from queries.directors import DirectorIn, DirectorOut, DirectorRepo


router = APIRouter()


@router.post("/api/directors", response_model=DirectorOut)
async def create_director(
    director: DirectorIn,
    repo: DirectorRepo = Depends()
):
    return repo.create_director(director)


@router.get("/api/directors", response_model=dict)
async def get_all_directors(
    repo: DirectorRepo = Depends()
):
    return repo.get_all_directors()
