from fastapi import (
    APIRouter,
    Request,
    Response,
    Depends,
)
from pydantic import BaseModel
from queries.users import (
    UserIn,
    UserOut,
    UserRepo,
)
from jwtdown_fastapi.authentication import Token
from authenticator import authenticator

router = APIRouter()


class AccountForm(BaseModel):
    username: str
    password: str


class AccountToken(Token):
    account: UserOut


class HttpError(BaseModel):
    detail: str


@router.post("/api/users", response_model=AccountToken)
async def create_user(
    user: UserIn,
    request: Request,
    response: Response,
    repo: UserRepo = Depends(),
):
    hashed_password = authenticator.hash_password(user.password)
    # turn this into a try/except later for duplicate account errors
    account = repo.create_user(user, hashed_password)
    # except DuplicateAccountError:
    #     raise HTTPException(
    #         status_code = status.HTTP_400_BAD_REQUEST,
    #         detail = "Cannot create an accoutn with those credentials"
    #     )
    form = AccountForm(username=user.username, password=user.password)
    token = await authenticator.login(response, request, form, repo)
    return AccountToken(account=account, **token.dict())


@router.get("/api/users/{username}", response_model=UserOut)
def get_user(
    username: str,
    response: Response,
    repo: UserRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
) -> UserOut:
    return repo.get_user(username)


@router.get("/api/users", response_model=dict)
def get_all_users(
    response: Response,
    repo: UserRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
) -> dict:
    users_data = repo.get_all_users()
    return users_data


@router.put("/api/users/{user_id}", response_model=AccountToken)
async def update_user(
    user_id: int,
    user: UserIn,
    request: Request,
    response: Response,
    repo: UserRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    hashed_password = authenticator.hash_password(user.password)
    account = repo.update_user(user_id, user, hashed_password)
    form = AccountForm(username=user.username, password=user.password)
    token = await authenticator.login(response, request, form, repo)
    return AccountToken(account=account, **token.dict())


@router.delete("/api/users/{user_id}", response_model=dict)
def delete_user(
    user_id: int,
    repo: UserRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
) -> dict:
    return repo.delete_user(user_id)


@router.get("/token", response_model=dict | None)
async def get_token(
    request: Request,
    account: UserOut = Depends(authenticator.try_get_current_account_data)
) -> AccountToken | None:
    print("ACCOUNT:", account)
    if account and authenticator.cookie_name in request.cookies:
        return {
            "access_token": request.cookies[authenticator.cookie_name],
            "type": "Bearer",
            "account": account,
        }
