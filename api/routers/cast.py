from fastapi import APIRouter, Depends
from queries.cast import ActorIn, ActorOut, ActorRepo


router = APIRouter()


@router.post("/api/cast", response_model=ActorOut)
async def create_actor(
    actor: ActorIn,
    repo: ActorRepo = Depends()
):
    return repo.create_actor(actor)


@router.get("/api/cast", response_model=dict)
async def get_all_actors(
    repo: ActorRepo = Depends()
) -> dict:
    return repo.get_all_actors()
