from fastapi import APIRouter, Depends
from queries.review_comments import ReviewComments
from authenticator import authenticator

router = APIRouter()


@router.get("/api/review_comments/{review_id}", response_model=dict)
def get_review_comments(
    review_id: int,
    repo: ReviewComments = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data)
) -> dict:
    return repo.get_review_comments(review_id)
