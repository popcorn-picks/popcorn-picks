steps = [
    [
        # "Up" SQL statement
        """
        ALTER TABLE directors
        RENAME COLUMN name TO director_name;
        """,
        """
        DROP TABLE users;
        """
    ],
    [
        """
        ALTER TABLE "cast"
        RENAME COLUMN name TO actor_name;
        """,
        """
        DROP TABLE users;
        """
    ]
]
