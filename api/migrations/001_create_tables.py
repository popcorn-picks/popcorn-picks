steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE users (
            id SERIAL PRIMARY KEY NOT NULL,
            first_name VARCHAR NOT NULL,
            last_name VARCHAR NOT NULL,
            username VARCHAR NOT NULL,
            profile_pic VARCHAR NOT NULL,
            date_joined DATE NOT NULL,
            password VARCHAR NOT NULL,
            active BOOL NOT NULL
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE users;
        """
    ],
    [
        # "Up" SQL statement
        """
        CREATE TYPE rating AS ENUM('G','PG','PG-13','R','NC-17');

        CREATE TABLE movies (
            id SERIAL PRIMARY KEY NOT NULL,
            title VARCHAR NOT NULL,
            picture VARCHAR NOT NULL,
            description TEXT NOT NULL,
            release_date DATE NOT NULL,
            upload_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
            rating rating NOT NULL,
            added_by INT REFERENCES users(id)
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE movies;
        """
    ],
    [
        # "Up" SQL statement
        """
        CREATE TYPE popcorn AS ENUM('1','2','3','4','5');

        CREATE TABLE reviews (
            id SERIAL PRIMARY KEY NOT NULL,
            title VARCHAR NOT NULL,
            review TEXT NOT NULL,
            rating popcorn,
            date_added DATE NOT NULL,
            user_id INT REFERENCES users(id),
            movie_id INT REFERENCES movies(id)
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE reviews;
        """
    ],
    [
        # "Up" SQL statement
        """
        CREATE TABLE "cast" (
            id SERIAL PRIMARY KEY NOT NULL,
            name VARCHAR NOT NULL,
            movie_id INT REFERENCES movies(id)
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE cast;
        """
    ],
    [
        # "Up" SQL statement
        """
        CREATE TABLE directors (
            id SERIAL PRIMARY KEY NOT NULL,
            name VARCHAR NOT NULL,
            movie_id INT REFERENCES movies(id)
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE directors;
        """
    ],
    [
        # "Up" SQL statement
        """
        CREATE TABLE comments (
            id SERIAL PRIMARY KEY NOT NULL,
            comment TEXT NOT NULL,
            user_id INT REFERENCES users(id),
            date_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
            review_id INT REFERENCES reviews(id)
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE comments;
        """
    ]
]
