steps = [
    [
        # "Up" SQL statement
        """
        ALTER TABLE comments
        DROP COLUMN review_id;
        ALTER TABLE comments
        ADD COLUMN review_id INT REFERENCES reviews(id) ON DELETE CASCADE;
        """,
        """
        DROP TABLE users;
        """
    ]
]
