steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE friends (
            "user" VARCHAR REFERENCES users(username),
            friend VARCHAR REFERENCES users(username)
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE friends;
        """
    ]
]
