steps = [
    [
        # "Up" SQL statement
        """
        ALTER TABLE directors
        DROP COLUMN movie_id;
        ALTER TABLE directors
        ADD COLUMN movie_id INT REFERENCES movies(id) ON DELETE CASCADE;
        """,
        """
        DROP TABLE users;
        """
    ]
]
