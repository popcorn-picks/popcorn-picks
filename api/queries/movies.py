from pydantic import BaseModel
from datetime import date
from queries.pool import pool


class MovieIn(BaseModel):
    title: str
    picture: str
    description: str
    release_date: date
    upload_date: date
    rating: str


class MovieOut(BaseModel):
    id: int
    title: str
    picture: str
    description: str
    release_date: date
    upload_date: date
    rating: str
    added_by: int


class MovieOutNoAddedBy(BaseModel):
    id: int
    title: str
    picture: str
    description: str
    release_date: date
    upload_date: date
    rating: str


class MovieRepo:
    def record_to_movie_out(self, record):
        movie_dict = {
            "id": record[0],
            "title": record[1],
            "picture": record[2],
            "description": record[3],
            "release_date": record[4],
            "upload_date": record[5],
            "rating": record[6],
            "added_by": record[7]
        }

        return movie_dict

    def create_movie(self, user_id: int, movie: MovieIn) -> MovieOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    INSERT INTO movies
                        (title,
                        picture,
                        description,
                        release_date,
                        upload_date,
                        rating,
                        added_by)
                    VALUES
                        (%s, %s, %s, %s, %s, %s, %s)
                    RETURNING
                    id,
                    title,
                    picture,
                    description,
                    release_date,
                    upload_date,
                    rating,
                    added_by;
                    """,
                    [
                        movie.title,
                        movie.picture,
                        movie.description,
                        movie.release_date,
                        movie.upload_date,
                        movie.rating,
                        user_id
                    ]
                )
                record = result.fetchone()
                return self.record_to_movie_out(record)

    def get_all_movies(self) -> dict:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id
                        ,title
                        ,picture
                        ,added_by
                        ,release_date
                        FROM movies
                        """
                    )
                    records = result.fetchall()
                    movies_list = [
                        {
                            "movie_id": record[0],
                            "title": record[1],
                            "picture": record[2],
                            "added_by": record[3],
                            "release_date": record[4]
                        }
                        for record in records
                    ]
                    return {"movies": movies_list}
        except Exception as e:
            print(e)
            return {"message": "Could not retrieve reviewed movies"}

    def get_movie_details(self, id: int) -> MovieOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id
                        , title
                        , picture
                        , description
                        , release_date
                        , upload_date
                        , rating
                        , added_by
                        FROM movies
                        WHERE id = %s
                        """,
                        [id]
                    )
                    record = result.fetchone()
                    print(record)
                    return self.record_to_movie_out(record)
        except Exception as e:
            print(e)
            return {"message": "Could not retrieve movie details"}

    def update_movie(self, movie_id: int, movie: MovieIn) -> MovieOutNoAddedBy:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    UPDATE movies
                    SET title = %s,
                        upload_date = %s,
                        release_date = %s,
                        rating = %s,
                        picture = %s,
                        description = %s
                    WHERE id = %s
                    """,
                    [
                        movie.title,
                        movie.upload_date,
                        movie.release_date,
                        movie.rating,
                        movie.picture,
                        movie.description,
                        movie_id
                    ]
                )
                old_movie = movie.dict()
                return MovieOutNoAddedBy(
                    id=movie_id,
                    **old_movie
                )

    def delete_movie(self, movie_id: int) -> dict:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM movies
                        WHERE id = %s
                        """,
                        [movie_id]
                    )
                    return {"message": "Movie has been deleted"}
        except Exception as e:
            print(e)
            return {"message": "Unable to delete movie"}
