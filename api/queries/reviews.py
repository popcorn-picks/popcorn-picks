from pydantic import BaseModel
from queries.pool import pool
from datetime import date


class ReviewIn(BaseModel):
    title: str
    review: str
    rating: str
    date_added: date
    movie_id: int


class ReviewOut(BaseModel):
    id: int
    title: str
    review: str
    rating: str
    date_added: date
    user_id: int
    movie_id: int


class ReviewRepo:
    def record_to_review_out(self, record):
        review_dict = {
            "id": record[0],
            "title": record[1],
            "review": record[2],
            "rating": record[3],
            "date_added": record[4],
            "user_id": record[5],
            "movie_id": record[6]
        }

        return review_dict

    def create_review(self, user_id: int, review: ReviewIn) -> ReviewOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    INSERT INTO reviews
                        (title,
                        review,
                        rating,
                        date_added,
                        user_id,
                        movie_id)
                    VALUES
                        (%s, %s, %s, %s, %s, %s)
                    RETURNING
                    id,
                    title,
                    review,
                    rating,
                    date_added,
                    user_id,
                    movie_id;
                    """,
                    [
                        review.title,
                        review.review,
                        review.rating,
                        review.date_added,
                        user_id,
                        review.movie_id,
                    ]
                )
                id = result.fetchone()[0]
                return ReviewOut(
                    id=id,
                    title=review.title,
                    review=review.review,
                    rating=review.rating,
                    date_added=review.date_added,
                    user_id=user_id,
                    movie_id=review.movie_id,
                )

    def get_all_reviews(self) -> dict:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id
                        , title
                        , review
                        , rating
                        , date_added
                        , user_id
                        , movie_id
                        FROM reviews
                        """
                    )
                    records = result.fetchall()
                    reviews_list = [
                        {
                            "id": record[0],
                            "title": record[1],
                            "review": record[2],
                            "rating": record[3],
                            "date_added": record[4],
                            "user_id": record[5],
                            "movie_id": record[6],
                        }
                        for record in records
                    ]
                    return {"reviews": reviews_list}
        except Exception as e:
            print(e)
            return {"message": "Could not retrieve reviews"}

    def get_review_details(self, id: int) -> ReviewOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id
                        , title
                        , review
                        , rating
                        , date_added
                        , user_id
                        , movie_id
                        FROM reviews
                        WHERE id = %s
                        """,
                        [id]
                    )
                    record = result.fetchone()
                    return self.record_to_review_out(record)
        except Exception as e:
            print(e)
            return {"message": "Could not retrieve review"}

    def update_review(self, review_id: int, review: ReviewIn) -> ReviewOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    UPDATE reviews
                    SET title = %s
                    , review = %s
                    , rating = %s
                    , date_added = %s
                    , user_id = %s
                    , movie_id = %s
                    WHERE id = %s
                    """,
                    [
                        review.title,
                        review.review,
                        review.rating,
                        review.date_added,
                        review.user_id,
                        review.movie_id,
                        review_id
                    ]
                )
                old_review = review.dict()
                return ReviewOut(
                    id=review_id,
                    **old_review
                )

    def delete_review(self, review_id: int) -> dict:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM reviews
                        WHERE id = %s
                        """,
                        [review_id]
                    )
                    return {"message": "Review has been deleted"}
        except Exception as e:
            print(e)
            return {"message": "Unable to delete review"}
