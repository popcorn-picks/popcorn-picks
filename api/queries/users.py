from pydantic import BaseModel
from queries.pool import pool
from datetime import date


class UserIn(BaseModel):
    first_name: str
    last_name: str
    username: str
    profile_pic: str
    date_joined: date
    password: str
    active: bool


class UserOut(BaseModel):
    id: int
    first_name: str
    last_name: str
    username: str
    profile_pic: str
    date_joined: date
    active: bool


class UserOutWithPassword(UserOut):
    hashed_password: str


class UserRepo:
    def record_to_user_out(self, record):
        user_dict = {
            "id": record[0],
            "first_name": record[1],
            "last_name": record[2],
            "username": record[3],
            "profile_pic": record[4],
            "date_joined": record[5],
            "password": record[6],
            "active": record[7]
        }

        return user_dict

    def create_user(self, user: UserIn, hashed_password: str) -> UserOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    INSERT INTO users
                        (first_name,
                        last_name,
                        username,
                        profile_pic,
                        date_joined,
                        password,
                        active)
                    VALUES
                        (%s, %s, %s, %s, %s, %s, %s)
                    RETURNING
                    id,
                    first_name,
                    last_name,
                    username,
                    profile_pic,
                    date_joined,
                    password,
                    active;
                    """,
                    [
                        user.first_name,
                        user.last_name,
                        user.username,
                        user.profile_pic,
                        user.date_joined,
                        hashed_password,
                        user.active,
                    ]
                )
                id = result.fetchone()[0]
                return UserOut(
                    id=id,
                    first_name=user.first_name,
                    last_name=user.last_name,
                    username=user.username,
                    profile_pic=user.profile_pic,
                    date_joined=user.date_joined,
                    active=user.active,
                )

    def get_user(self, username: str) -> UserOutWithPassword:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id
                        , first_name
                        , last_name
                        , username
                        , profile_pic
                        , date_joined
                        , password
                        , active
                        FROM users
                        WHERE username = %s
                        """,
                        [username]
                    )
                    record = result.fetchone()
                    print("DATEJOINED:", record)
                    return self.record_to_user_out(record)
        except Exception as e:
            print(e)
            return {"message": "Could not retrieve user"}

    def get_all_users(self) -> dict:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id
                        , username
                        , profile_pic
                        FROM users
                        """
                    )
                    records = result.fetchall()
                    users_list = [
                        {
                            "id": record[0],
                            "username": record[1],
                            "profile_picture": record[2],
                        }
                        for record in records
                    ]
                    return {"users": users_list}
        except Exception as e:
            print(e)
            return {"message": "Could not retrieve users"}

    def update_user(
            self,
            user_id: int,
            user: UserIn,
            hashed_password: str) -> UserOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE users
                        SET
                            first_name = %s,
                            last_name = %s,
                            username = %s,
                            password = %s,
                            profile_pic = %s
                        WHERE id = %s
                        """,
                        [
                            user.first_name,
                            user.last_name,
                            user.username,
                            hashed_password,
                            user.profile_pic,
                            user_id
                        ]
                    )
                    old_user = user.dict()
                    return UserOut(
                        id=user_id,
                        **old_user
                    )
        except Exception as e:
            print(e)
            return {"message": "Could not update user"}

    def delete_user(self, user_id: int) -> dict:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM users
                        WHERE id = %s
                        """,
                        [user_id]
                    )
                    return {"message": "User deleted."}
        except Exception as e:
            print(e)
            return {"message": "Was unable to delete user."}
