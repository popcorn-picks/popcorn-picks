from pydantic import BaseModel
from queries.pool import pool


class ActorIn(BaseModel):
    actor_name: str
    movie_id: int


class ActorOut(BaseModel):
    id: int
    actor_name: str
    movie_id: int


class ActorRepo:
    def record_to_actor_out(self, record):
        actor_dict = {
            "id": record[0],
            "actor_name": record[1],
            "movie_id": record[2]
        }
        return actor_dict

    def create_actor(self, actor: ActorIn) -> ActorOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    INSERT INTO "cast"
                        (actor_name,
                        movie_id
                        )
                    VALUES
                        (%s,%s)
                    RETURNING
                    id,
                    actor_name,
                    movie_id;
                    """,
                    [
                        actor.actor_name,
                        actor.movie_id
                    ]
                )
                record = result.fetchone()
                return self.record_to_actor_out(record)

    def get_all_actors(self) -> dict:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT actor_name
                        FROM "cast"
                        """
                    )
                    records = result.fetchall()
                    actors_list = [
                        {
                            "actor_name": record[0]
                        } for record in records
                    ]
                    return {"actors": actors_list}
        except Exception as e:
            print(e)
            return {"message": "Could not retrieve actors."}
