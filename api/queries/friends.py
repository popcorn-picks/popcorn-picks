from pydantic import BaseModel
from queries.pool import pool


class Friends(BaseModel):
    user: str
    friend: str


class FriendRepo:
    def record_to_friends(self, record):
        friend_dict = {
            "user": record[0],
            "friend": record[1]
        }
        return friend_dict

    def add_friend(self, user: str, friend: str) -> Friends:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    INSERT INTO friends
                        ("user",
                        friend)
                    VALUES
                        (%s, %s)
                    RETURNING
                    "user",
                    friend
                    """,
                    [
                        user,
                        friend
                    ]
                )
                record = result.fetchone()
                return self.record_to_friends(record)
