from pydantic import BaseModel
from queries.pool import pool


class DirectorIn(BaseModel):
    director_name: str
    movie_id: int


class DirectorOut(BaseModel):
    id: int
    director_name: str
    movie_id: int


class DirectorRepo:
    def record_to_director_out(self, record):
        director_dict = {
            "id": record[0],
            "director_name": record[1],
            "movie_id": record[2]
        }
        return director_dict

    def create_director(self, director: DirectorIn) -> DirectorOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    INSERT INTO directors
                        (director_name,
                        movie_id
                        )
                    VALUES
                        (%s,%s)
                    RETURNING
                    id,
                    director_name,
                    movie_id;
                    """,
                    [
                        director.director_name,
                        director.movie_id
                    ]
                )
                record = result.fetchone()
                return self.record_to_director_out(record)

    def get_all_directors(self) -> dict:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT director_name
                        FROM directors
                        """
                    )
                    records = result.fetchall()
                    directors_list = [
                        {
                            "director_name": record[0]
                        } for record in records
                    ]
                    return {"directors": directors_list}
        except Exception as e:
            print(e)
            return {"message": "could not retrieve all directors."}
