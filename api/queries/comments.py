from pydantic import BaseModel
from datetime import date
from queries.pool import pool


class CommentIn(BaseModel):
    comment: str
    # user_id: int
    date_time: date
    review_id: int


class CommentOut(BaseModel):
    id: int
    comment: str
    user_id: int
    date_time: date
    review_id: int


class CommentRepo:
    def record_to_comment_out(self, record):
        comment_dict = {
            "id": record[0],
            "comment": record[1],
            "user_id": record[2],
            "date_time": record[3],
            "review_id": record[4]
        }

        return comment_dict

    def create_comment(self, user_id: int, comment: CommentIn) -> CommentOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    INSERT INTO comments
                        (comment,
                        user_id,
                        date_time,
                        review_id)
                    VALUES
                        (%s, %s, %s, %s)
                    RETURNING
                        id,
                        comment,
                        user_id,
                        date_time,
                        review_id
                    """,
                    [
                        comment.comment,
                        user_id,
                        comment.date_time,
                        comment.review_id
                    ]
                )
                id = result.fetchone()[0]
                return CommentOut(
                    id=id,
                    comment=comment.comment,
                    user_id=user_id,
                    date_time=comment.date_time,
                    review_id=comment.review_id
                    )

    def get_all_comments(self) -> dict:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id, comment, user_id, date_time, review_id
                        FROM comments
                        ORDER BY date_time;
                        """
                    )
                    records = result.fetchall()
                    comments_list = [
                        {
                            "id": record[0],
                            "comment": record[1],
                            "user_id": record[2],
                            "date_time": record[3],
                            "review_id": record[4]
                        }
                        for record in records
                    ]
                    return {"comments": comments_list}
        except Exception as e:
            print(e)
            return {"message": "Could not get all comments for this review"}

    def get_comment_details(self, id: int) -> CommentOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id
                        , comment
                        , user_id
                        , date_time
                        , review_id
                        FROM comments
                        WHERE id = %s
                        """,
                        [id]
                    )
                    record = result.fetchone()
                    return self.record_to_comment_out(record)
        except Exception as e:
            print(e)
            return {"message": "Could not retrieve comment"}

    def update_comment(self,
                       comment_id: int,
                       comment: CommentIn) -> CommentOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    UPDATE comments
                    SET comment = %s
                    WHERE id = %s
                    """,
                    [
                        comment.comment,
                        comment_id
                    ]
                )
                old_comment = comment.dict()
                return CommentOut(
                        id=comment_id,
                        **old_comment
                )

    def delete_comment(self, comment_id: int) -> dict:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM comments
                        WHERE id = %s
                        """,
                        [comment_id]
                    )
                    return {"message": "Comment deleted."}
        except Exception as e:
            print(e)
            return {"message": "Was unable to delete comment."}
