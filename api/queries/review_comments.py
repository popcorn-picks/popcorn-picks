from queries.pool import pool


class ReviewComments:
    def get_review_comments(self, review_id: int) -> dict:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                            SELECT
                            c.comment AS comment,
                            c.user_id AS user_id,
                            c.date_time AS date_time,
                            c.review_id AS review_id
                            FROM comments c
                            INNER JOIN reviews r
                            ON c.review_id = r.id
                            WHERE r.id = %s
                        """,
                        [review_id]
                    )
                    records = result.fetchall()
                    review_comments = [
                        {
                            "comment": record[0],
                            "user_id": record[1],
                            "date_time": record[2],
                            "review_id": record[3]
                        }
                        for record in records
                    ]
                    return {"review_comments": review_comments}
        except Exception as e:
            print(e)
            return {"message": "Could not retrieve the review's comments"}
