# Popcorn Picks

Welcome to Popcorn Picks - Your Ultimate Movie Rating Website!

Creators:
- Angelika Villapando
- Harut Sarkisyan
- Troy Accos
- Roland Manvelyan

## Overview

Popcorn Picks is the go-to platform for movie enthusiasts to share and discover their favorite films. Tailor your movie-watching experience by creating an account, listing your favorite movies, writing reviews, and engaging in discussions through comments. With a sleek design and functionalities, Popcorn Picks is more than just a movie rating website – it's your personalized cinematic journey.

## Design

- [API Endpoints / Design](https://gitlab.com/popcorn-picks/popcorn-picks/-/blob/development/docs/apis.md?ref_type=heads)
- [Data Models](https://gitlab.com/popcorn-picks/popcorn-picks/-/blob/development/docs/data-model.md?ref_type=heads)
- [Web Design](https://gitlab.com/popcorn-picks/popcorn-picks/-/blob/development/docs/ghi.md?ref_type=heads)


## Intended Market

Popcorn Picks is targeting general consumers in the movie enthusiast market. It caters to individuals who want a personalized and tailored experience in discovering, rating, and discussing movies. Whether you're a casual moviegoer or a dedicated cinephile, Popcorn Picks is designed for you.

## Functionality

- User Account and Login:
    - Users have the ability to create an account and log in, granting access to their personalized profile and additional features not available to non-logged in users.

- Logged-in User Experience:
    - Upon logging in, users are directed to the Home Page, where an expanded navigation bar provides access to additional features.

- Navigation Bar Elements:
    - The enhanced navigation bar for logged-in users includes three primary elements:
        - Movies:
            - Users can click on the "Movies" button to navigate to a page displaying a comprehensive list of movies. These movies may be listed by the logged-in user or other users within the application.
        - Users:
            - Users can click on the "Users" button to navigate to a page displaying a list of users of the application
        - Profile:
            - Users can click on their profile picture displayed on the navigation bar to either view their profile details, or log out of their account.
            - The profile details consist of information of the user, such as all their reviews that have been posted (that can be deleted by the user), as well as the user's full name and username.

- Movies List and Creation:
    - The "Movies" section not only presents a curated list of movies but also offers users the option to contribute by creating their own movie listings.
    - Users can initiate the creation of a new movie by completing a form with the required fields:
        - Title
        - Picture
        - Description
        - Release Date
        - Rating
        - Director(s)

- Movie Details:
    - Users can click on each individual movie listed to access a dedicated page containing comprehensive movie details.
    - Movie details include essential information such as title, picture, description, release date, rating, and details about the directors associated with the movie.

- Movie Review Feature:
    - On the movie detail page, users have the option to express their thoughts by reviewing the movie.
    - A dedicated "Review" button is available, leading users to a review form containing fields for a popcorn rating, review title, and the detailed review itself.
    - This interactive feature allows users to share their opinions and contribute to the community's collective insights on each movie.

## Project Initialization

To pull down and get started with Popcorn Picks locally, please follow these steps:

1. Clone the repository down to your local machine
2. Navigate to the project directory
3. Run 'docker volume create popcorn-data'
4. Run 'docker compose build'
5. Run 'docker compose up'
6. Go to localhost3000 and enjoy!

To check out our deployed website, click this link!: https://popcorn-picks.gitlab.io/popcorn-picks 